package org.example;

import org.example.POO.Capitulo1.Polimorfismo.PolimorfismoEstatico.Ejemplo1.Cientifica;

public class Main {
    public static void main(String[] args) {
        Cientifica cientifica = new Cientifica("Cassio");

        System.out.println("La marca de la calculadore es :" + cientifica + " y la suma es : " + cientifica.sumar(3, 2));
    }
}