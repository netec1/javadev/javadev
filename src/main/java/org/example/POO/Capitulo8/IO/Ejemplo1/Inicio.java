package org.example.POO.Capitulo8.IO.Ejemplo1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Inicio {
    public static void main(String[] args) {
        String urlArchivo = "/home/carlos/Documentos/textoPrueba.txt";

        try (BufferedReader reader = new BufferedReader(new FileReader(urlArchivo))) {
            String linea;
            int totalLineas = 0;
            System.out.println("---Inicio de archivo---");
            while ((linea = reader.readLine()) != null) {
                // Divide la línea en espacios para contar las palabras
                int numeroPalabras = linea.trim().isEmpty() ? 0 : linea.split("\\s+").length;
                System.out.println(linea + " (Palabras: " + numeroPalabras + ")");
                totalLineas++;
            }
            System.out.println("---Fin de archivo---");
            System.out.println("Total de líneas: " + totalLineas);
        } catch (IOException ex) {
            System.out.println("Algo pasó: " + ex.getMessage());
        }
    }
}
