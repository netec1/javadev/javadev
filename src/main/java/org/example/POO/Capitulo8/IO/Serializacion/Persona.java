package org.example.POO.Capitulo8.IO.Serializacion;

import java.io.Serializable;

public class Persona implements Serializable {

    private static final long serialVersionUID = 12L;
    private String nombre;
    private int edad;

    private transient String ciudad;
    private char sexo;

    public Persona(String nombre, int edad, String ciudad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.ciudad = ciudad;
        this.sexo = sexo;
    }
    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                '}';
    }
}
