package org.example.POO.Capitulo8.IO.Serializacion.Ejemplo1;

import org.example.POO.Capitulo8.IO.Serializacion.Persona;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Inicio {
    public static void main(String[] args) {
        String urlArchivo = "/home/carlos/Documentos/textoPrueba_copia5.txt";

        Persona p1 = new Persona("Edgardo", 28, "CDMX", 'M');

        try (FileOutputStream fos = new FileOutputStream(urlArchivo);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            oos.writeObject(p1);
            System.out.println("Serialización completa");
        } catch (IOException ex) {
            System.out.println("Algo paso con la serialización: " + ex);
        }
    }
}
