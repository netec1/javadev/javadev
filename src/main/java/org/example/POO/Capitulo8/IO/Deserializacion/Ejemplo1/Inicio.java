package org.example.POO.Capitulo8.IO.Deserializacion.Ejemplo1;

import org.example.POO.Capitulo8.IO.Serializacion.Persona;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Inicio {
    public static void main(String[] args) {
        String urlArchivo = "/home/carlos/Documentos/textoPrueba_copia5.txt";

        try (FileInputStream fis = new FileInputStream(urlArchivo);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            Persona pDes = (Persona) ois.readObject();
            System.out.println("---Persona deserializada---");
            System.out.println(pDes);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Algo pasó con la deserialización: " + ex);
        }

    }
}
