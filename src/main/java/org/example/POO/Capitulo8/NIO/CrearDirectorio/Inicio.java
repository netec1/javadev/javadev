package org.example.POO.Capitulo8.NIO.CrearDirectorio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {
        String baseDir = "/home/carlos/Descargas";
        Path url = Paths.get(baseDir).resolve("NuevoDirectorio");

        try {
            Files.createDirectory(url);


            Files.createDirectories(url.resolve("Subdir1/Subdir2/Subdir3"));
            System.out.println("Directorios creados en: " + url);
        } catch (IOException ex) {
            System.out.println("Error al crear directorios: " + ex);
        }
    }
}
