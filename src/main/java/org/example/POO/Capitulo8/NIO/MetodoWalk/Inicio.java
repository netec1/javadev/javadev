package org.example.POO.Capitulo8.NIO.MetodoWalk;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Inicio {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        Path urlArchivo = Paths.get(home).resolve("Descargas");

        try (Stream<Path> contenido = Files.walk(urlArchivo)) {
            contenido.filter(t -> t.toString().endsWith(".pdf"))
                    .forEach(System.out::println);
        } catch (IOException ex) {
            System.out.println("Error al buscar en raíz: " + ex);
        }
    }
}
