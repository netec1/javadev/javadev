package org.example.POO.Capitulo8.NIO.LecturaArchivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Inicio {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        Path urlArchivo = Paths.get(home).resolve("prueba.txt");

        try (Stream<String> lineas = Files.lines(urlArchivo)) {
            System.out.println("--Inicio de archivo--");
            lineas.forEach(System.out::println);
            System.out.println("--fin de archivo--");
        } catch (IOException ex) {
            System.out.println("Error al leer el archivo: " + ex);
        }
    }
}
