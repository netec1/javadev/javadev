package org.example.POO.Capitulo8.NIO.MoverArchivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {
        Path urlOriginal = Paths.get("/home/carlos/Documentos/textoPrueba_copia5.txt");
        Path urlDestino = Paths.get("/home/carlos/Documentos/CarpetaNueva");

        System.out.println("Original: " + urlOriginal);
        System.out.println("Destino: " + urlDestino);

        try {
            Files.createDirectories(urlDestino);

            // Moviendo el archivo
            if (Files.exists(urlOriginal)) {
                Files.move(urlOriginal, urlDestino.resolve(urlOriginal.getFileName()));
                System.out.println("Se ha movido el archivo");
            } else {
                System.out.println("El archivo original no existe");
            }
        } catch (IOException ex) {
            System.out.println("Error al mover: " + ex);
        }
    }
}
