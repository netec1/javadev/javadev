package org.example.POO.Capitulo8.NIO.ExistenciaArchivo;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {

        String urlArchivo = "/home/carlos/Documentos/textoPrueba_copia53.txt";


        Path path = Paths.get(urlArchivo);


        System.out.println("URL: " + path);


        System.out.println("¿El archivo/directorio existe?: " + Files.exists(path));
    }
}
