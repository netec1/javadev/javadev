package org.example.POO.Capitulo8.NIO.EscrituraArchivo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Inicio {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        Path urlArchivo = Paths.get(home).resolve("prueba.txt");

        List<String> lineas = new ArrayList<>();
        lineas.add("linea1");
        lineas.add("linea2");
        lineas.add("linea3");
        lineas.add("linea4");
        lineas.add("Hola Clase");

        try {
            Files.write(urlArchivo, lineas, StandardOpenOption.APPEND);
            System.out.println("archivo escrito");
        } catch (IOException ex) {
            System.out.println("Error al escribir: " + ex);
        }
    }
}
