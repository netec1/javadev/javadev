package org.example.POO.Capitulo8.NIO.Ejemplo1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Inicio {
    public static void main(String[] args) {
        String urlArchivoOrigen = "/home/carlos/Documentos/textoPrueba.txt";
        String urlArchivoDestino = "/home/carlos/Documentos/textoPrueba_copia23.txt";

        try (FileChannel channelOrigen = new FileInputStream(urlArchivoOrigen).getChannel();
             FileChannel channelDestino = new FileOutputStream(urlArchivoDestino).getChannel()) {

            // Se crea un buffer para obtener el contenido del archivo
            ByteBuffer buffer = ByteBuffer.allocate((int) channelOrigen.size());
            // Se llena el buffer con contenido del archivo de origen
            channelOrigen.read(buffer);
            // Se prepara el buffer para la escritura leyendo desde el inicio
            buffer.flip();
            // Se copia el contenido del buffer al canal de salida
            channelDestino.write(buffer);

            System.out.println("Contenido copiado");
        } catch (IOException ex) {
            System.out.println("Ocurrió un error: " + ex.getMessage());
        }
    }
}
