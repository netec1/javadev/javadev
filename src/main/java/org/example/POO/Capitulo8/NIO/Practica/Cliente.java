package org.example.POO.Capitulo8.NIO.Practica;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    private int id;
    private List<String> cuentas;

    public Cliente(int id) {
        this.id = id;
        this.cuentas = new ArrayList<>();
    }

    public void addCuenta(String cuenta) {
        cuentas.add(cuenta);
    }

    public int getNumeroDeCuentas() {
        return cuentas.size();
    }

    public String toString() {
        return "Cliente " + id + ": " + cuentas;
    }
}
