package org.example.POO.Capitulo8.NIO.MetodoList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Inicio {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        Path urlArchivo = Paths.get(home);

        try (Stream<Path> contenido = Files.list(urlArchivo)) {
            contenido.filter(t -> t.toString().endsWith(".txt"))
                    .forEach(System.out::println);
        } catch (IOException ex) {
            System.out.println("Error al buscar en raíz: " + ex);
        }
    }
}
