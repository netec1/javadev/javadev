package org.example.POO.Capitulo8.NIO.CrearArchivo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class Inicio {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        String nombreArchivo = UUID.randomUUID().toString().concat(".txt");
        Path url = Paths.get(home).resolve(nombreArchivo);

        System.out.println("Archivo: " + url);

        try {
            Files.createFile(url);
            System.out.println("Archivo creado");
        } catch (IOException ex) {
            System.out.println("Error al crear archivo: " + ex);
        }
    }
}
