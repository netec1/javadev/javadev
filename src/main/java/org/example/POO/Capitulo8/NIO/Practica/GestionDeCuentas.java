package org.example.POO.Capitulo8.NIO.Practica;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GestionDeCuentas {
    public static void main(String[] args) {
        Cliente[] clientes = {
                new Cliente(1),
                new Cliente(2),
                new Cliente(3)
        };

        String archivoCuentas = "ruta/a/el/archivo/descargado.txt";

        try {
            List<String> lineas = Files.readAllLines(Paths.get(archivoCuentas));
            for (String linea : lineas) {
                String[] partes = linea.split(":"); // Asumiendo que las cuentas están separadas por ":"
                int idCliente = Integer.parseInt(partes[0]);
                String cuenta = partes[1];
                clientes[idCliente - 1].addCuenta(cuenta);
            }

            for (Cliente cliente : clientes) {
                System.out.println(cliente);
            }

            // Validación de la cantidad de cuentas por cliente
            if (clientes[0].getNumeroDeCuentas() != 1 || clientes[1].getNumeroDeCuentas() != 2 || clientes[2].getNumeroDeCuentas() != 3) {
                System.out.println("Error en la asignación de cuentas");
            }

        } catch (Exception e) {
            System.out.println("Error al procesar el archivo: " + e.getMessage());
        }
    }
}
