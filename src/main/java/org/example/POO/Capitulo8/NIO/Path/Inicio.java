package org.example.POO.Capitulo8.NIO.Path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {
        Path path = Paths.get("/home/carlos/Documentos/textoPrueba_copia3.txt");

        System.out.println("Nombre del archivo (getFileName): " + path.getFileName());
        System.out.println("Nombre del primer componente de la ruta (getName(0)): " + path.getName(0));
        System.out.println("Cantidad de componentes en la ruta (getNameCount): " + path.getNameCount());
        System.out.println("Subruta desde el índice 0 hasta 2 (subpath): " + path.subpath(0, path.getNameCount()));
        System.out.println("Directorio padre (getParent): " + path.getParent());
        System.out.println("Componente raíz de la ruta (getRoot): " + path.getRoot());
        System.out.println("Ruta absoluta (toAbsolutePath): " + path.toAbsolutePath());
        System.out.println("Conversión a objeto File (toFile): " + path.toFile());
        System.out.println("Ruta normalizada (normalize): " + path.normalize());
        System.out.println("Comparación con otra ruta (compareTo): " + path.compareTo(Paths.get("/home/carlos/Documentos")));
        System.out.println("Resolución con otro nombre de archivo (resolve): " + path.resolve("archivoAdicional.txt"));
    }
}

