package org.example.POO.Capitulo8.NIO.CopiarArchivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {
        Path urlOriginal = Paths.get("/home/carlos/Documentos/textoPrueba.txt");
        Path urlDestino = Paths.get("/home/carlos/Documentos/NuevoDirectorio");

        System.out.println("Original: " + urlOriginal);
        System.out.println("Destino: " + urlDestino);

        try {
            if (Files.exists(urlOriginal) && Files.exists(urlDestino)) {
                Files.copy(urlOriginal, urlDestino.resolve(urlOriginal.getFileName()));
                System.out.println("Se ha copiado el archivo");
            } else {
                System.out.println("El archivo original o el destino no existen");
            }
        } catch (IOException ex) {
            System.out.println("Error al copiar: " + ex);
        }
    }
}
