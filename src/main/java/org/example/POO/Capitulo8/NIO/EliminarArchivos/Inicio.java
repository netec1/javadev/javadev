package org.example.POO.Capitulo8.NIO.EliminarArchivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Inicio {
    public static void main(String[] args) {
        Path urlArchivo = Paths.get("/home/carlos/Documentos/CarpetaNueva/textoPrueba_copia5.txt");

        try {
            if (Files.exists(urlArchivo)) {
                Files.delete(urlArchivo);
                System.out.println("Se ha eliminado el archivo");
            } else {
                System.out.println("El archivo no existe");
            }
        } catch (IOException ex) {
            System.out.println("Error al eliminar: " + ex);
        }
    }
}
