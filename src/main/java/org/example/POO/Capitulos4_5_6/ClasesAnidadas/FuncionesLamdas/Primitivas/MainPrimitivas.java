package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.Primitivas;

import java.util.function.ToIntFunction;

public class MainPrimitivas {
    public static void main(String[] args) {
        ToIntFunction<String> parseToInt = s -> Integer.parseInt(s);
        System.out.println(parseToInt.applyAsInt("123")); // Imprime 123
    }
}
