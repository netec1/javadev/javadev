package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Funcionales.Ejemplo1;


public class Main {
    public static void main(String[] args) {

        Funciones funciones = new Funciones() {
            @Override
            public double operacion(double a, double b) {
                return a-b;
            }
        };

        System.out.println(funciones.operacion(3.0 , 2.0 ));
    }
}
