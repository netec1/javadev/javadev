package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Ejemplo1;

public class Outer {
    private String outerField = "Outside";

    public class Inner {
        public void show() {
            // Acceso directo al campo de la clase externa
            System.out.println(outerField);
        }
    }
}


