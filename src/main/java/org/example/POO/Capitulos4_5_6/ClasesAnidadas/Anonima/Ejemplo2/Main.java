package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Ejemplo2;



public class Main {

    public static void main(String[] args) {
        ClaseExterna claseExterna = new ClaseExterna();

        ClaseExterna.ClaseInterna claseInterna = claseExterna.new ClaseInterna();

        System.out.println(claseExterna.x * claseInterna.z);
    }



}
