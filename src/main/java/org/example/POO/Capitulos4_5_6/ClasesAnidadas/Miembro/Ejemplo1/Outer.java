package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Miembro.Ejemplo1;

public class Outer {

    public void invoke() {
        perform(new Handler() {
            @Override
            public void handle() {
                System.out.println("Handled anonymously");
            }
        });
    }

    public void perform(Handler handler) {
        handler.handle();
    }

    public interface Handler {
        void handle();
    }
}
