package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Funcionales.Ejemplo2;



public class Main {
    public static void main(String[] args) {
        Funcion<Integer, Integer, Integer> suma = (a, b) -> a + b;
        System.out.println("Suma : " + suma.operacion(1, 2));
    }
}
