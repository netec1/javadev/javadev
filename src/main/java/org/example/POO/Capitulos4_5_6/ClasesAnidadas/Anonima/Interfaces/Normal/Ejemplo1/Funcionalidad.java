package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Normal.Ejemplo1;


public interface Funcionalidad {
    double operacion(double a, double b);
    boolean buscar(String donde, String que);
}
