package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.EjerciciosPracticas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class MainConsumer {
    public static void main(String[] args) {
        Consumer<String> printName = name -> System.out.println(name);
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

        names.forEach(printName);

    }
}
