package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Estatica.Ejemplo1;

public class Outer {
    private static String staticField = "Static Outside";

    public static class StaticInner {
        public void display() {
            // Acceso directo al campo estático de la clase externa
            System.out.println(staticField);
        }
    }
}

