package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas;

import java.util.function.UnaryOperator;

public class MainUnaryOperator {
    public static void main(String[] args) {
        UnaryOperator<String> upperCaseOperator = s -> s.toUpperCase();
        System.out.println(upperCaseOperator.apply("hello"));

    }
}
