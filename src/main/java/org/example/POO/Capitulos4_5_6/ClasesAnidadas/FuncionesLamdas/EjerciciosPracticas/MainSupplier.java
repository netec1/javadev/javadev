package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.EjerciciosPracticas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class MainSupplier {
    public static void main(String[] args) {
        Supplier<Double> randomSupplier = () -> Math.random();
        List<Double> randomNumbers = new ArrayList<>();


        for (int i = 0; i < 5; i++) {
            randomNumbers.add(randomSupplier.get());
        }
        System.out.println(randomNumbers);

    }
}
