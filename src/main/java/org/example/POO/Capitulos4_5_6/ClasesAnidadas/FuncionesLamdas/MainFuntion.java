package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas;

import java.util.function.Function;

public class MainFuntion {
    public static void main(String[] args) {
        Function<String, Integer> lengthFunction = s -> s.length();
        System.out.println(lengthFunction.apply("Hello World"));
    }
}
