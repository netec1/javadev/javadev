package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.EjerciciosPracticas;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class MainUnaryOperator {
    public static void main(String[] args) {
        UnaryOperator<String> toUpperCase = s -> s.toUpperCase();
        List<String> words = Arrays.asList("apple", "banana", "cherry");

        List<String> upperCaseWords = words.stream().map(toUpperCase).collect(Collectors.toList());
        System.out.println(upperCaseWords);

    }
}
