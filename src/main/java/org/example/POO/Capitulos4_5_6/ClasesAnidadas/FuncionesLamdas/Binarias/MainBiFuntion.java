package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.Binarias;

import java.util.function.BiFunction;

public class MainBiFuntion {
    public static void main(String[] args) {
        BiFunction<String, String, String> concat = (a, b) -> a + b;
        System.out.println(concat.apply("Hello ", "World"));

    }
}
