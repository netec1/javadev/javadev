package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas;

import java.util.function.Consumer;

public class MainConsumer {
    public static void main(String[] args) {
        Consumer<String> printer = message -> System.out.println(message);
        printer.accept("Hello Java");

    }
}

// Ejercicio en clase para los que vayan entrando, tienen hasta las 5 c:
//Crear un Predicate que verifique si un número es par.
//Usar un Consumer para imprimir los nombres de una lista.
//Transformar una lista de números a sus cuadrados usando Function.
//Generar una lista de números aleatorios usando Supplier.
//Convertir una lista de cadenas a mayúsculas usando UnaryOperator.
