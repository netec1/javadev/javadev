package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.EjerciciosPracticas;

import java.util.function.Predicate;

public class MainPredicate {
    public static void main(String[] args) {
        Predicate<Integer> isEven = number -> number % 2 == 0;

        // Probemos el Predicate con algunos números
        System.out.println(isEven.test(10)); // Debería imprimir "true"
        System.out.println(isEven.test(3));  // Debería imprimir "false"

    }
}
