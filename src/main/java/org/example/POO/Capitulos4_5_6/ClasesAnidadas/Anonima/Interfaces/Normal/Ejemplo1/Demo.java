package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Normal.Ejemplo1;


public class Demo {
    public static void main(String[] args) {
        Funcionalidad f = new Funcionalidad() {
            @Override
            public double operacion(double a, double b) {
                return a + b;
            }

            @Override
            public boolean buscar(String donde, String que) {
                return donde.contains(que);
            }
        };
        Funcionalidad f2 = new Funcionalidad() {
            @Override
            public double operacion(double a, double b) {
                return a * b;
            }

            @Override
            public boolean buscar(String donde, String que) {
                return donde.contains(que);
            }
        };

        System.out.println(f.operacion(1, 2));
        System.out.println(f2.operacion(2, 3));

        System.out.println(f.buscar("Hola mundo","Mundo"));
        System.out.println("Resultado de buscar(\"hola mundo\", \"mundo\"): " + f.buscar("hola mundo", "mundo"));
    }
}