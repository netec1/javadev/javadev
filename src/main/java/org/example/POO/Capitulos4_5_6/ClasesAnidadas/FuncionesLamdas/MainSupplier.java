package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas;

import java.util.function.Supplier;

public class MainSupplier {
    public static void main(String[] args) {

        Supplier<Double> randomSupplier = () -> Math.random();
        System.out.println(randomSupplier.get());

    }
}
