package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas;

import java.util.function.Predicate;

public class MainPredicate {
    public static void main(String[] args) {
        Predicate<String> isLongText = text -> text.length() > 10;
        System.out.println(isLongText.test("Hello World"));
    }

}
