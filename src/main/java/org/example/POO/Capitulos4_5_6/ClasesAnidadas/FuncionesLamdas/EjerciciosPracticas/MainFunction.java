package org.example.POO.Capitulos4_5_6.ClasesAnidadas.FuncionesLamdas.EjerciciosPracticas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MainFunction {
    public static void main(String[] args) {
        Function<Integer, Integer> squareFunction = number -> number * number;
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        // Transformamos y recogemos los resultados
        List<Integer> squares = numbers.stream().map(squareFunction).collect(Collectors.toList());
        System.out.println(squares); // Debería imprimir [1, 4, 9, 16, 25]

    }
}
