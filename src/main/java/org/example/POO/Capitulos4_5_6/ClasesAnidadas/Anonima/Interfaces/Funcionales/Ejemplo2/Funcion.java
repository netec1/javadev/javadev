package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Funcionales.Ejemplo2;

@FunctionalInterface
public interface Funcion<T, U, R> {
    R operacion(T a, U b);
}
