package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Interfaces.Funcionales.Ejemplo1;

@FunctionalInterface
public interface Funciones {
    double operacion(double a, double b);
}

