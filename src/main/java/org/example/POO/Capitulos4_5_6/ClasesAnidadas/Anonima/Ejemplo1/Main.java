package org.example.POO.Capitulos4_5_6.ClasesAnidadas.Anonima.Ejemplo1;


public class Main {
    public static void main(String[] args) {
        // Para instanciar la clase Inner, primero se debe crear una instancia de Outer.
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.show();

    }
}
