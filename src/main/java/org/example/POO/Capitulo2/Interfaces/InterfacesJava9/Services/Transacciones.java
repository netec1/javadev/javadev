package org.example.POO.Capitulo2.Interfaces.InterfacesJava9.Services;

public interface Transacciones {
    default void logTransaccion(String mensaje) {
        System.out.println("Transacción: " + mensaje);
    }

    default void depositar(double cantidad) {
        if (cantidad > 0) {
            logTransaccion("depositado " + cantidad);
            System.out.println("Depósito exitoso.");
        } else {
            System.out.println("Depósito fallido: cantidad inválida.");
        }
    }

    default void retirar(double cantidad) {
        if (cantidad > 0) {
            logTransaccion("retirado " + cantidad);
            System.out.println("Retiro exitoso.");
        } else {
            System.out.println("Retiro fallido: cantidad inválida.");
        }
    }
}


