package org.example.POO.Capitulo2.Interfaces.InterfaceFuncional.Services;

@FunctionalInterface
public interface Converter {
    int convert(String value);

    default void log(String data) {
        System.out.println("Logging: " + data);
    }
}


