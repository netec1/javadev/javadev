package org.example.POO.Capitulo2.Interfaces.InterfaceFuncional;

import org.example.POO.Capitulo2.Interfaces.InterfaceFuncional.Services.Converter;

public class TestConverter {
    public static void main(String[] args) {
        Converter stringToInteger = new Converter() {
            @Override
            public int convert(String s) {
                return Integer.parseInt(s);
            }

            @Override
            public void log(String data) {
                Converter.super.log(data + " - from anonymous class");
            }
        };

        System.out.println(stringToInteger.convert("12345"));
        stringToInteger.log("Test completed");
    }
}


