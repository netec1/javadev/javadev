package org.example.POO.Capitulo2.Interfaces.InterfacesJava8.Services;

public interface ServicioBanco {

    //Constante
    public static final double CARGO_POR_SOBREGIRO = 45;

    //Metodo default
    public default double verificarDeposito(double amount, int pin){

        //Verificar pin
        //Verificar que el monto sea mayor a 0
        return 0.0;
    }

    public default double verificarRetiro(double amount, int pin){
        //Verificar pin
        //Verificar que el monto sea mayor a 0
        //Verificar que el balance de la cuenta no sea negativa
        return 0.0;
    }

}
