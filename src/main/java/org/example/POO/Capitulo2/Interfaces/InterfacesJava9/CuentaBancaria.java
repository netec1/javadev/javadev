package org.example.POO.Capitulo2.Interfaces.InterfacesJava9;

import org.example.POO.Capitulo2.Interfaces.InterfacesJava9.Services.Transacciones;

public class CuentaBancaria implements Transacciones {
    private double saldo;

    public CuentaBancaria(double saldoInicial) {
        this.saldo = saldoInicial;
    }

    // Sobreescribe los métodos default para incluir la lógica de saldo
    public void depositar(double cantidad) {
        if (cantidad > 0) {
            saldo += cantidad;
            Transacciones.super.depositar(cantidad); // Llama al método default
        } else {
            System.out.println("Depósito fallido: cantidad inválida.");
        }
    }

    public void retirar(double cantidad) {
        if (cantidad > 0 && saldo >= cantidad) {
            saldo -= cantidad;
            Transacciones.super.retirar(cantidad); // Llama al método default
        } else {
            System.out.println("Retiro fallido: cantidad insuficiente.");
        }
    }
}

