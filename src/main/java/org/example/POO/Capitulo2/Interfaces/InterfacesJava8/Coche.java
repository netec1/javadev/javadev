package org.example.POO.Capitulo2.Interfaces.InterfacesJava8;

import org.example.POO.Capitulo2.Interfaces.InterfacesJava8.Services.Vehiculo;

public class Coche implements Vehiculo {
    public void mover() {
        System.out.println("El coche se mueve adelante.");
    }


    public static void main(String[] args) {
        Coche coche = new Coche();

        coche.mover();
    }
}


