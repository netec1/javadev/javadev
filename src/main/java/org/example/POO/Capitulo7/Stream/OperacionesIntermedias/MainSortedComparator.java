package org.example.POO.Capitulo7.Stream.OperacionesIntermedias;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.ArrayList;
import java.util.List;

public class MainSortedComparator {
    public static void main(String[] args) {
        List<Persona> lista = new ArrayList<>();

        lista.add(new Persona("Zarai", 27, "EDOMEX", 'F'));
        lista.add(new Persona("Carolina", 18, "EDOMEX", 'F'));
        lista.add(new Persona("Victor", 30, "CDMX", 'M'));
        lista.add(new Persona("Javier", 32, "CDMX", 'M'));
        lista.add(new Persona("Edgar", 28, "NL", 'M'));
        lista.add(new Persona("Daniela", 36, "SON", 'F'));
        lista.add(new Persona("Luz", 25, "PUES", 'F'));
        lista.add(new Persona("Ernesto", 13, "OAX", 'M'));
        lista.add(new Persona("Alicia", 29, "QRO", 'F'));
        lista.add(new Persona("Jorge", 41, "VER", 'M'));


        System.out.println("ordenado por edad");
        lista.stream()
                .sorted((t,s)->((Integer)t.getEdad()).compareTo(s.getEdad()))
                .forEach(t->System.out.println(t));
    }
}
