package org.example.POO.Capitulo7.Stream.EjerciciosPractica;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.List;
import java.util.Optional;

public class BusquedaCondicional {
    public static void main(String[] args) {
        List<Persona> personas = List.of(
                new Persona("Juan", 31, "CDMX"),
                new Persona("Maria", 29, "Guadalajara"),
                new Persona("Carlos", 35, "CDMX"),
                new Persona("Ana", 27, "Monterrey")
        );

        Optional<Persona> resultado = personas.stream()
                .filter(p -> p.getCiudad().equals("CDMX") && p.getEdad() > 30)
                .findAny();
        resultado.ifPresent(System.out::println);
    }
}
