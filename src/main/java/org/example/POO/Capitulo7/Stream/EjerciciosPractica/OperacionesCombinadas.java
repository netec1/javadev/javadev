package org.example.POO.Capitulo7.Stream.EjerciciosPractica;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.List;
import java.util.stream.Collectors;

public class OperacionesCombinadas {
    public static void main(String[] args) {
        List<Persona> personas = List.of(
                new Persona("Juan", 24),
                new Persona("Maria", 29),
                new Persona("Carlos", 34),
                new Persona("Ana", 27)
        );

        List<String> resultado = personas.stream()
                .filter(p -> p.getEdad() > 25)
                .map(Persona::getNombre)
                .sorted()
                .collect(Collectors.toList());

        System.out.println(resultado);
    }
}
