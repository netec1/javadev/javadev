package org.example.POO.Capitulo7.Stream.EjerciciosPractica;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AgrupacionParticion {
    public static void main(String[] args) {
        List<Persona> personas = List.of(
                new Persona("Juan", 31, "CDMX"),
                new Persona("Maria", 29, "Guadalajara"),
                new Persona("Carlos", 35, "CDMX"),
                new Persona("Ana", 27, "Monterrey")
        );

        Map<String, List<Persona>> agrupadosPorCiudad = personas.stream()
                .collect(Collectors.groupingBy(Persona::getCiudad));

        Map<Boolean, List<Persona>> porEdad = personas.stream()
                .collect(Collectors.partitioningBy(p -> p.getEdad() >= 30));

        System.out.println("Agrupados por ciudad: " + agrupadosPorCiudad);
        System.out.println("Menores y mayores de 30: " + porEdad);
    }
}
