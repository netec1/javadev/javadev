package org.example.POO.Capitulo7.Stream.EjerciciosPractica;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.List;
import java.util.stream.Collectors;

public class OrdenacionDepuracion {
    public static void main(String[] args) {
        List<Persona> personas = List.of(
                new Persona("Juan", 24),
                new Persona("Maria", 29),
                new Persona("Carlos", 34),
                new Persona("Ana", 27)
        );

        personas.stream()
                .sorted((p1, p2) -> Integer.compare(p1.getEdad(), p2.getEdad()))
                .peek(System.out::println)
                .collect(Collectors.toList());
    }
}
