package org.example.POO.Capitulo7.Stream.OpeFinalCortoCircuito;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class MainTakeWhile {
    public static void main(String[] args) {
        List<Persona> lista = new ArrayList<>();

        lista.add(new Persona("Zarai", 27, "EDOMEX", 'F'));
        lista.add(new Persona("Carolina", 18, "EDOMEX", 'F'));
        lista.add(new Persona("Victor", 30, "CDMX", 'M'));
        lista.add(new Persona("Javier", 32, "CDMX", 'M'));
        lista.add(new Persona("Edgar", 28, "NL", 'M'));
        lista.add(new Persona("Daniela", 36, "SON", 'F'));
        lista.add(new Persona("Luz", 25, "PUES", 'F'));
        lista.add(new Persona("Ernesto", 13, "OAX", 'M'));
        lista.add(new Persona("Alicia", 29, "QRO", 'F'));
        lista.add(new Persona("Jorge", 41, "VER", 'M'));

        System.out.println("Las primeras personas que son de sexo 'F'?");
        Stream<Persona> respuesta = lista.stream()
                .takeWhile(t->t.getSexo() == 'F');

        respuesta.forEach(System.out::println);
    }
}
