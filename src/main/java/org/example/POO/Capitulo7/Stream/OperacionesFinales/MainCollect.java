package org.example.POO.Capitulo7.Stream.OperacionesFinales;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainCollect {
    public static void main(String[] args) {
        List<Persona> lista = new ArrayList<>();

        lista.add(new Persona("Zarai", 27, "EDOMEX", 'F'));
        lista.add(new Persona("Carolina", 18, "EDOMEX", 'F'));
        lista.add(new Persona("Victor", 30, "CDMX", 'M'));
        lista.add(new Persona("Javier", 32, "CDMX", 'M'));
        lista.add(new Persona("Edgar", 28, "NL", 'M'));
        lista.add(new Persona("Daniela", 36, "SON", 'F'));
        lista.add(new Persona("Luz", 25, "PUES", 'F'));
        lista.add(new Persona("Ernesto", 13, "OAX", 'M'));
        lista.add(new Persona("Alicia", 29, "QRO", 'F'));
        lista.add(new Persona("Jorge", 41, "VER", 'M'));


        //Personas que tiene mas de 18
        List<Persona>  listaP = lista.stream()
                .filter(t->t.getEdad()>18)
                .collect(Collectors.toList());


        //Personas agrupadas por ciudad
        Map<String, List<Persona>> mapa1 = lista.stream()
                .collect(Collectors
                        .groupingBy(t->t.getCiudad()));

        //Personas que tiene mas de 25 y personas que no
        Map<Boolean, List<Persona>> mapa2 = lista.stream()
                .collect(Collectors.partitioningBy(t->t.getEdad()>25));

        System.out.println(listaP);
        System.out.println(mapa1);
        System.out.println(mapa2);
    }



   // Filtrar y Mostrar:
//
   // Crea un stream de personas y utiliza filter() para mostrar solo las personas mayores de 25 años.
//
   //         Usa forEach() para imprimir los nombres de estas personas.
//
   // Transformación y Agregación:
//
   // Utiliza map() para obtener los nombres de todas las personas en el stream, pero convertidos a mayúsculas.
//
   // Recolecta estos nombres en una lista usando collect().
//
   // Ordenación y Depuración:
//
   // Ordena una lista de personas por edad utilizando sorted() y muestra cada paso del proceso con peek().
//
   // Búsqueda Condicional:
//
   // Dada una lista de personas, utiliza findAny() para encontrar cualquier persona que sea de "CDMX" y tenga más de 30 años.
//
   // Agrupación y Partición:
//
   // Usa collect() con groupingBy() para agrupar personas por ciudad de origen.
//
   //         Utiliza partitioningBy() para separar personas en dos grupos: aquellos con edad menor de 30 y los de 30 o más.
//
   // Operaciones Combinadas:
//
   // Pide a los estudiantes que creen un stream que filtre, transforme y ordene datos de una lista de personas, utilizando filter(), map(), y sorted() respectivamente, y luego recolecten el resultado en una lista.









}
