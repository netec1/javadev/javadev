package org.example.POO.Capitulo7.Stream.OperacionesIntermedias;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.ArrayList;
import java.util.List;

public class MainPeek {
    public static void main(String[] args) {
        List<Persona> lista = new ArrayList<>();
        List<Persona> pEDOMEX = new ArrayList<>();

        lista.add(new Persona("Zarai", 27, "EDOMEX", 'F'));
        lista.add(new Persona("Carolina", 18, "EDOMEX", 'F'));
        lista.add(new Persona("Victor", 30, "CDMX", 'M'));
        lista.add(new Persona("Javier", 32, "CDMX", 'M'));
        lista.add(new Persona("Edgar", 28, "NL", 'M'));
        lista.add(new Persona("Daniela", 36, "SON", 'F'));
        lista.add(new Persona("Luz", 25, "PUES", 'F'));
        lista.add(new Persona("Ernesto", 13, "OAX", 'M'));
        lista.add(new Persona("Alicia", 29, "QRO", 'F'));
        lista.add(new Persona("Jorge", 41, "VER", 'M'));

        lista.stream()
                .filter(t->t.getCiudad().equals("EDOMEX"))
                .peek(t->System.out.println(t))
                .forEach(t->pEDOMEX.add(t));
        System.out.println("fin stream");
    }
}
