package org.example.POO.Capitulo7.Stream.EjerciciosPractica;

import org.example.POO.Capitulo7.Stream.Persona;

import java.util.List;

public class FiltrarMostrar {
    public static void main(String[] args) {
        List<Persona> personas = List.of(
                new Persona("Juan", 24),
                new Persona("Maria", 29),
                new Persona("Carlos", 34),
                new Persona("Ana", 27)
        );

        personas.stream()
                .filter(persona -> persona.getEdad() > 25)
                .forEach(persona -> System.out.println(persona.getNombre()));
    }
}
