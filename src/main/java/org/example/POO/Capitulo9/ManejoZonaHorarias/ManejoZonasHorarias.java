package org.example.POO.Capitulo9.ManejoZonaHorarias;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ManejoZonasHorarias {
    public static void main(String[] args) {
        ZonedDateTime fechaHoraZonificada = ZonedDateTime.now(ZoneId.of("America/Mexico_City"));
        System.out.println("Fecha y hora zonificada: " + fechaHoraZonificada);
    }
}
