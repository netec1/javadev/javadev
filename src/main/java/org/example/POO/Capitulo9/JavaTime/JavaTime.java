package org.example.POO.Capitulo9.JavaTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class JavaTime {
    public static void main(String[] args) {
        // Fecha actual
        LocalDate fechaActual = LocalDate.now();
        System.out.println("Fecha actual: " + fechaActual);

        // Hora actual
        LocalTime horaActual = LocalTime.now();
        System.out.println("Hora actual: " + horaActual);

        // Fecha y hora actuales
        LocalDateTime fechaHoraActual = LocalDateTime.now();
        System.out.println("Fecha y hora actuales: " + fechaHoraActual);
    }
}
