package org.example.POO.Capitulo9.Practica;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZonaHorarias {
    public static void main(String[] args) {
        ZonedDateTime fechaHoraParis = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        ZonedDateTime fechaHoraTokyo = ZonedDateTime.now(ZoneId.of("Asia/Tokyo"));
        ZonedDateTime fechaHoraNuevaYork = ZonedDateTime.now(ZoneId.of("America/New_York"));

        System.out.println("Fecha y hora en Nueva York: " + fechaHoraNuevaYork);
        System.out.println("Fecha y hora en Paris: " + fechaHoraParis);
        System.out.println("Fecha y hora en Tokyo: " + fechaHoraTokyo);

    }
}
