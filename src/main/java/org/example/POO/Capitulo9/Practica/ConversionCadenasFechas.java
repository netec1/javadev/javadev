package org.example.POO.Capitulo9.Practica;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ConversionCadenasFechas {

    public static void main(String[] args) {
        String fechaCadena = "10/12/2023";
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecha = LocalDate.parse(fechaCadena, formato);
        System.out.println("Fecha convertida : " + fecha);
    }
}
