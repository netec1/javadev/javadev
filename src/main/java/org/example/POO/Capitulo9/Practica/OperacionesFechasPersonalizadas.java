package org.example.POO.Capitulo9.Practica;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class OperacionesFechasPersonalizadas {
    public static void main(String[] args) {
        LocalDateTime fechaHoraPersonalizada = LocalDateTime.of(2024, Month.JUNE, 4, 15, 40, 21);
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDateTime fechaMas4Dias = fechaHoraPersonalizada.plusDays(4);
        System.out.println("Fecha mas 4 dias: " + fechaMas4Dias.format(formato));

        LocalDateTime fechaMenos2Meses = fechaHoraPersonalizada.minusMonths(2);
        System.out.println("Fecha menos 2 meses : " + fechaMenos2Meses.format(formato));

    }
}
