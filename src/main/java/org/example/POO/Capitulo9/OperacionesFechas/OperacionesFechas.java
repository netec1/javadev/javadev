package org.example.POO.Capitulo9.OperacionesFechas;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class OperacionesFechas {
    public static void main(String[] args) {
        LocalDateTime fechaInicial = LocalDateTime.of(2021, Month.DECEMBER, 31, 23, 29);

        // Sumar días
        LocalDateTime fechaMasDias = fechaInicial.plusDays(5);
        System.out.println("Fecha más 5 días: " + fechaMasDias);

        // Restar días
        LocalDateTime fechaMenosDias = fechaInicial.minusDays(5);
        System.out.println("Fecha menos 5 días: " + fechaMenosDias);

        // Cuantas horas faltan para el 31 de diciembre
        long horasHastaFinAnio = fechaInicial.until(fechaInicial.plusDays(5), ChronoUnit.HOURS);
        System.out.println("Horas hasta el 31 de diciembre: " + horasHastaFinAnio);
    }
}
