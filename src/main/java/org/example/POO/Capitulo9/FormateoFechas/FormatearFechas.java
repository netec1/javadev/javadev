package org.example.POO.Capitulo9.FormateoFechas;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class FormatearFechas {
    public static void main(String[] args) {
        LocalDate fecha = LocalDate.of(2021, Month.SEPTEMBER, 2);

        // Formato predefinido
        DateTimeFormatter formatoISO = DateTimeFormatter.ISO_DATE;
        String fechaFormateadaISO = fecha.format(formatoISO);
        System.out.println("Fecha formateada ISO: " + fechaFormateadaISO);

        // Formato personalizado
        DateTimeFormatter formatoPersonalizado = DateTimeFormatter.ofPattern("dd 'de' MMMM 'del' yyyy");
        String fechaFormateadaPersonalizada = fecha.format(formatoPersonalizado);
        System.out.println("Fecha formateada personalizada: " + fechaFormateadaPersonalizada);
    }
}
