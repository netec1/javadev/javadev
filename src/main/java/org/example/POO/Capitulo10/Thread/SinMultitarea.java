package org.example.POO.Capitulo10.Thread;

public class SinMultitarea {
    public static void main(String[] args) {
        // Tarea 1: Simula una operación que toma 5 segundos
        procesarTarea("Tarea 1", 5000);

        // Tarea 2: Simula una operación que toma 3 segundos
        procesarTarea("Tarea 2", 3000);
    }

    public static void procesarTarea(String nombre, int duracion) {
        System.out.println(nombre + " iniciada.");
        try {
            Thread.sleep(duracion);
        } catch (InterruptedException e) {
            System.out.println("Error en " + nombre + ": " + e.getMessage());
        }
        System.out.println(nombre + " completada.");
    }
}

