package org.example.POO.Capitulo10.Thread;

public class ConMultitarea {
    public static void main(String[] args) {
        // Crear hilos para cada tarea
        Thread tarea1 = new Thread(() -> procesarTarea("Tarea 1", 5000));
        Thread tarea2 = new Thread(() -> procesarTarea("Tarea 2", 3000));

        // Iniciar hilos
        tarea1.start();
        tarea2.start();
    }

    public static void procesarTarea(String nombre, int duracion) {
        System.out.println(nombre + " iniciada.");
        try {
            Thread.sleep(duracion);
        } catch (InterruptedException e) {
            System.out.println("Error en " + nombre + ": " + e.getMessage());
        }
        System.out.println(nombre + " completada.");
    }
}

