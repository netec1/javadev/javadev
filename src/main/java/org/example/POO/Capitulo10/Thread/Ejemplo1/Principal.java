package org.example.POO.Capitulo10.Thread.Ejemplo1;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Principal {
    public static void main(String[] args) {
        // Definición de las tareas
        Runnable tarea1 = () -> System.out.println("Ejecutando tarea1 en hilo: " + Thread.currentThread().getName());
        Runnable tarea2 = () -> System.out.println("Ejecutando tarea2 en hilo: " + Thread.currentThread().getName());
        Runnable tarea3 = () -> System.out.println("Ejecutando tarea3 en hilo: " + Thread.currentThread().getName());

        // Creación de un pool de hilos con 2 hilos
        ExecutorService pool = Executors.newFixedThreadPool(2);

        // Enviamos las tareas al pool de hilos
        pool.submit(tarea1);
        pool.submit(tarea2);
        pool.submit(tarea3);

        // Cerramos el pool indicando que no agregaremos más tareas al pool
        pool.shutdown();
    }
}
