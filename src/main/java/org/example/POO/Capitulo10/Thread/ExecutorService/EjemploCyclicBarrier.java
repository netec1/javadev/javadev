package org.example.POO.Capitulo10.Thread.ExecutorService;

import java.util.concurrent.CyclicBarrier;

public class EjemploCyclicBarrier {
    public static void main(String[] args) {
        final int NUM_HILOS = 3;
        CyclicBarrier barrier = new CyclicBarrier(NUM_HILOS, () -> {
            System.out.println("Todos los hilos han alcanzado la barrera.");
        });

        for (int i = 0; i < NUM_HILOS; i++) {
            new Thread(new MiRunnable(barrier)).start();
        }
    }
}
