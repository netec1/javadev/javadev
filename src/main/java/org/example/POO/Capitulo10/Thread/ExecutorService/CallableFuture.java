package org.example.POO.Capitulo10.Thread.ExecutorService;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableFuture {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        Callable<Integer> tarea = () -> {
            int suma = 0;
            for (int i = 0; i < 10; i++) {
                suma += i;
                Thread.sleep(500);
            }
            return suma;
        };

        try {
            Future<Integer> resultado = executor.submit(tarea);
            System.out.println("Resultado de la suma: " + resultado.get());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }
}
