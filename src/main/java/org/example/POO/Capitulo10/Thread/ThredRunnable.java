package org.example.POO.Capitulo10.Thread;

public class ThredRunnable {
    public static void main(String[] args) {
        Runnable tarea1 = () -> {
            System.out.println("Comenzando: " + Thread.currentThread().getName());
            int tmp = 0;
            for (int i = 0; i < 100; i++) {
                tmp += i;
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    System.out.println("Error al esperar: " + e);
                }
            }
            System.out.println("La suma es: " + tmp + " en " + Thread.currentThread().getName());
        };

        Thread hilo1 = new Thread(tarea1);
        hilo1.start();
    }
}
