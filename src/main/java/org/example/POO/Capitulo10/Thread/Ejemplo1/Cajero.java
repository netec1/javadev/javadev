package org.example.POO.Capitulo10.Thread.Ejemplo1;

import org.example.POO.Capitulo10.Cliente;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

// Para crear un hilo extendemos de Thread
public class Cajero extends Thread {
    private String nombre;
    private Cliente cl;

    public Cajero(String nombre, Cliente cl) {
        this.nombre = nombre;
        this.cl = cl;
    }

    @Override
    public void run() {
        procesandoCompra();
    }

    public void procesandoCompra() {
        LocalTime horaActual = LocalTime.now();
        System.out.println("La cajera " + this.nombre + " comienza a procesar el carrito del cliente "
                + cl.getNombre() + " a las " + horaActual);
        for (int i = 0; i < cl.getProductos().length; i++) {
            esperarProceso();
            System.out.println("Se ha procesado el producto: " + (i + 1) + " del cliente " + cl.getNombre());
        }
        System.out.println("La cajera " + this.nombre + " ha finalizado de procesar el carrito de comprar de "
                + cl.getNombre() + " en "
                + ChronoUnit.SECONDS.between(horaActual, LocalTime.now()) + " segundos");
    }

    private void esperarProceso() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Error al esperar: " + e);
        }
    }
}

