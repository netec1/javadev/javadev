package org.example.POO.Capitulo10.Thread.Ejemplo1;

import org.example.POO.Capitulo10.Cliente;

import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        LocalTime inicioPrograma = LocalTime.now();

        // Se crean dos clientes
        Cliente cl1 = new Cliente("Edgar", new String[]{"pr1", "pr2", "pr3", "pr4"});
        Cliente cl2 = new Cliente("Carolina", new String[]{"pr2", "pr2", "pr3", "pr4"});

        // Se crea un cajero
        Cajero cr1 = new Cajero("Jaz", cl1);
        Cajero cr2 = new Cajero("Luz", cl2);

        // Se inician los dos hilos
        cr1.start();
        cr2.start();
    }
}
