package org.example.POO.Capitulo10.Thread.ExecutorService;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class MiRunnable implements Runnable {
    private CyclicBarrier barrier;

    public MiRunnable(CyclicBarrier barrier) {
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " está esperando en la barrera.");
            barrier.await();
            System.out.println(Thread.currentThread().getName() + " ha pasado la barrera.");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
