package org.example.POO.Capitulo3.Genericos.ClasesGenericas.Ejemplo1;

public class TestGenericos {
    public static void main(String[] args) {
        Container<String> stringContainer = new Container<>("Hello, World!");
        System.out.println("String Container: " + stringContainer.getContent());

        Container<Integer> integerContainer = new Container<>(123);
        System.out.println("Integer Container: " + integerContainer.getContent());

        Container<Double> doubleContainer = new Container<>(9.75);
        System.out.println("Double Container: " + doubleContainer.getContent());
    }
}
