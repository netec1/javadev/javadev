package org.example.POO.Capitulo3.Genericos.ColeccionesApi.Map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MainMap {
    public static void main(String[] args) {
        // HashMap no mantiene un orden específico de inserción
        Map<Integer, String> hashMap = new HashMap<>();
        hashMap.put(3, "Tres");
        hashMap.put(2, "Dos");
        hashMap.put(1, "Uno");

        System.out.println("HashMap: " + hashMap);

        // LinkedHashMap mantiene el orden de inserción
        Map<Integer, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(3, "Tres");
        linkedHashMap.put(2, "Dos");
        linkedHashMap.put(1, "Uno");

        System.out.println("LinkedHashMap: " + linkedHashMap);

        // TreeMap ordena las claves según el orden natural (o un Comparator si se proporciona)
        Map<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(3, "Tres");
        treeMap.put(2, "Dos");
        treeMap.put(1, "Uno");

        System.out.println("TreeMap: " + treeMap);
    }
}
