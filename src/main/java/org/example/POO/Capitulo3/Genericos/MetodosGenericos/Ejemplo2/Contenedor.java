package org.example.POO.Capitulo3.Genericos.MetodosGenericos.Ejemplo2;

public class Contenedor<T> {

    private T value;

    public Contenedor(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
