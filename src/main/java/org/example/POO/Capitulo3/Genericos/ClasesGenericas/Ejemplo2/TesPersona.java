package org.example.POO.Capitulo3.Genericos.ClasesGenericas.Ejemplo2;

public class TesPersona {

    public static void main(String[] args) {
        Persona<String, Integer> persona = new Persona<>("Carlos", 50);
        System.out.println("Nombre: " + persona.getPrimerValor() + " Edad : " + persona.getSegundoValor());

        Persona<Double, Character> altura = new Persona<>(180.3, 'M');
        System.out.println("Su altura es : " + altura.getPrimerValor() + " Su sexo es : " + altura.getSegundoValor() );
    }
}
