package org.example.POO.Capitulo3.Genericos.ColeccionesGenericos.Ejemplo1;

import java.util.ArrayList;
import java.util.List;

public class EjemploColecciones {
    public static void main(String[] args) {
        // Creación de una lista genérica de Personas
        List<Persona> listaPersonas = new ArrayList<>();

        // Agregar personas a la lista
        listaPersonas.add(new Persona("Juan", 25));
        listaPersonas.add(new Persona("Ana", 30));
        listaPersonas.add(new Persona("Carlos", 22));

        // Mostrar la lista de personas
        for (Persona persona : listaPersonas) {
            System.out.println(persona);
        }
    }
}

