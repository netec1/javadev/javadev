package org.example.POO.Capitulo3.Genericos.MetodosGenericos.Ejemplo1;

import java.util.Arrays;

public class GenericMethods {


    public static <T extends Comparable<T>> boolean compare(T item1, T item2) {
        return item1.compareTo(item2) == 0;
    }

    public static void main(String[] args) {
        // Prueba del método genérico con diferentes tipos de datos
        System.out.println(compare(10, 10)); // Devuelve true
        System.out.println(compare("hello", "world")); // Devuelve false
        System.out.println(compare(3.14, 3.14)); // Devuelve true
    }
}

