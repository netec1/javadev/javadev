package org.example.POO.Capitulo3.Genericos.ColeccionesApi.Queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class MainQueue {
    public static void main(String[] args) {
        // Uso de LinkedList como Queue
        Queue<String> linkedListQueue = new LinkedList<>();
        linkedListQueue.add("Manzana");
        linkedListQueue.add("Banana");
        linkedListQueue.add("Cereza");
        linkedListQueue.add("Dátil");

        System.out.println("Cola LinkedList: " + linkedListQueue);
        System.out.println("Elemento eliminado: " + linkedListQueue.remove());
        System.out.println("Cabeza de la cola después de eliminar: " + linkedListQueue.peek());

        // Uso de PriorityQueue
        Queue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(10);
        priorityQueue.add(20);
        priorityQueue.add(15);
        priorityQueue.add(5);

        System.out.println("\nCola de Prioridad: " + priorityQueue);
        System.out.println("Elemento eliminado: " + priorityQueue.poll());
        System.out.println("Cabeza de la cola después de eliminar: " + priorityQueue.peek());
    }
}
