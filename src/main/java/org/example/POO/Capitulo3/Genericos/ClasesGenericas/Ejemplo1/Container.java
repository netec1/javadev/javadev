package org.example.POO.Capitulo3.Genericos.ClasesGenericas.Ejemplo1;

public class Container<T> {
    private T content; // Variable de instancia de tipo T

    public Container() {
    }

    public Container(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}

