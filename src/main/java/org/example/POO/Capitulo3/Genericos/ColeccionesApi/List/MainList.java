package org.example.POO.Capitulo3.Genericos.ColeccionesApi.List;

import java.util.ArrayList;
import java.util.List;

public class MainList {
    public static void main(String[] args) {
        // Creando una lista de tipo ArrayList
        List<String> ciudades = new ArrayList<>();

        // Añadiendo elementos a la lista
        ciudades.add("Tokio");
        ciudades.add("Nueva York");
        ciudades.add("París");
        ciudades.add("Londres");
        ciudades.add("Londres");

        // Imprimiendo todos los elementos de la lista
        System.out.println("Ciudades en la lista:");
        for (String ciudad : ciudades) {
            System.out.println(ciudad);
        }
    }
}
