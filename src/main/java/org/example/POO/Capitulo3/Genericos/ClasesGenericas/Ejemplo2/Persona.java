package org.example.POO.Capitulo3.Genericos.ClasesGenericas.Ejemplo2;

public class Persona<T, V> {

    private T primerValor;
    private V segundoValor;

    public Persona(T primerValor, V segundoValor) {
        this.primerValor = primerValor;
        this.segundoValor = segundoValor;
    }

    public T getPrimerValor() {
        return primerValor;
    }

    public void setPrimerValor(T primerValor) {
        this.primerValor = primerValor;
    }

    public V getSegundoValor() {
        return segundoValor;
    }

    public void setSegundoValor(V segundoValor) {
        this.segundoValor = segundoValor;
    }
}
