package org.example.POO.Capitulo3.Genericos.MetodosGenericos.Ejemplo2;

public class TesContenedor {


    public static <T> void prueba(Contenedor<T> tContenedor1, Contenedor<T> tContenedor2 ){

        T tem = tContenedor1.getValue();
        tContenedor1.setValue(tContenedor2.getValue());
        tContenedor2.setValue(tem);

    }

    public static void main(String[] args) {
        Contenedor<Integer> contenedor1 = new Contenedor<>(1);
        Contenedor<Integer> contenedor2 = new Contenedor<>(2);

        System.out.println("Valores antes del intercambio : contenedor1 = "  + contenedor1.getValue() + ", contenedor2 + " + contenedor2.getValue() );
        prueba(contenedor1, contenedor2);
        System.out.println("Valores despues del intercambio : contenedor1 = "  + contenedor1.getValue() + ", contenedor2 + " + contenedor2.getValue() );

        Contenedor<String> contenedor3 = new Contenedor<>("HOLA");
        Contenedor<String> contenedor4 = new Contenedor<>("CLASE");

        System.out.println("Valores antes del intercambio : contenedor1 = "  + contenedor3.getValue() + ", contenedor2 + " + contenedor4.getValue() );
        prueba(contenedor3, contenedor4);
        System.out.println("Valores despues del intercambio : contenedor1 = "  + contenedor3.getValue() + ", contenedor2 + " + contenedor4.getValue() );

    }

}
