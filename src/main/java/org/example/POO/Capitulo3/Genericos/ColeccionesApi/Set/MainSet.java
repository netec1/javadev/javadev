package org.example.POO.Capitulo3.Genericos.ColeccionesApi.Set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class MainSet {
    public static void main(String[] args) {
        // HashSet no garantiza un orden específico
        Set<Integer> hashSet = new HashSet<>();
        hashSet.add(40);
        hashSet.add(30);
        hashSet.add(20);
        hashSet.add(10);
        System.out.println("HashSet (orden no garantizado): " + hashSet);

        // LinkedHashSet mantiene el orden de inserción
        Set<Integer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add(40);
        linkedHashSet.add(30);
        linkedHashSet.add(20);
        linkedHashSet.add(10);
        System.out.println("LinkedHashSet (orden de inserción): " + linkedHashSet);

        // TreeSet ordena los elementos en orden natural (ascendente)
        Set<Integer> treeSet = new TreeSet<>();
        treeSet.add(40);
        treeSet.add(30);
        treeSet.add(3);
        treeSet.add(20);
        treeSet.add(10);
        System.out.println("TreeSet (orden natural): " + treeSet);
    }
}
