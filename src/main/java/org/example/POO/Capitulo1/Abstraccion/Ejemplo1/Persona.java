package org.example.POO.Capitulo1.Abstraccion.Ejemplo1;

public class Persona {
    String nombre;
    int edad;
    String direccion;
    char sexo;

    public Persona() {
    }

    public Persona(String nombre, int edad, String direccion, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.direccion = direccion;
        this.sexo = sexo;
    }


    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                ", sexo=" + sexo +
                '}';
    }

    public static void main(String[] args) {

        Persona p= new Persona();

        p.nombre = "Juan";
        p.edad = 30;
        p.direccion = "123 Calle Principal";
        p.sexo = 'M';
        System.out.println(p.toString());
    }
}
