package org.example.POO.Capitulo1.Abstraccion.ClasesAbstractas.Ejemplo1;

public class Refrigerador extends Electrodomestico{

    public Refrigerador(String nombre, String marca, double precio) {
        super(nombre, marca, precio);
    }

    @Override
    public void prender() {
        System.out.println("Prendendo Refrigerador");
    }

    @Override
    public void apagar() {
        System.out.println("Apagar Refrigerador");
    }
}
