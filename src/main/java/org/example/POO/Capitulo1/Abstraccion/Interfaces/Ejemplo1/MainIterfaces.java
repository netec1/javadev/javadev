package org.example.POO.Capitulo1.Abstraccion.Interfaces.Ejemplo1;

public class MainIterfaces {

    public static void main(String[] args) {
        Electrodomestico electrodomestico = new Electrodomestico();

        electrodomestico.prender();
        electrodomestico.apagar();
    }
}
