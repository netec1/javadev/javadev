package org.example.POO.Capitulo1.Abstraccion.Interfaces.Ejemplo1;

public interface Funcion {

    void prender();
    void apagar();
}
