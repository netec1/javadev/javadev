package org.example.POO.Capitulo1.Abstraccion.Ejemplo2;

public class MainPersona {

    public static void main(String[] args) {

        Persona persona = new Persona("Edgardo",28,"CDMX",'M');

        Persona persona2 = new Persona("Carolina", 24, "EDOMEX",'F');

        Persona persona3 = new Persona("Liz", 39, "SON",'F');


       System.out.println(persona);
       System.out.println(persona2);
       System.out.println(persona3);
    }

}
