package org.example.POO.Capitulo1.Abstraccion.Ejemplo2;

public class Persona {

    String nombre;
    int edad;
    String direccion;
    char sexo;

    public Persona() {
    }

    public Persona(String nombre, int edad, String direccion, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.direccion = direccion;
        this.sexo = sexo;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                ", sexo=" + sexo +
                '}';
    }
}
