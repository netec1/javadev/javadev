package org.example.POO.Capitulo1.Abstraccion.Interfaces.Ejemplo1;

public class Electrodomestico implements Funcion{
    @Override
    public void prender() {
        System.out.println("Prendendo Electrodomestico");
    }

    @Override
    public void apagar() {
        System.out.println("Apagando Electrodomestico");
    }


}
