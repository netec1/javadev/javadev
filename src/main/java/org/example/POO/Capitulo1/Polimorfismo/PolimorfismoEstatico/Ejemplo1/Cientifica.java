package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoEstatico.Ejemplo1;

public class Cientifica extends Calculadora{


    public Cientifica(String marca) {
        super(marca);
    }

    public int suma(int [] numeros) {

        int suma = 0;

        for (int varTmp: numeros) {
            suma += varTmp;
        }
        return suma;
    }

}
