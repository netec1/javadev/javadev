package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoEstatico.Ejemplo1;

public class Calculadora {

    private String marca;

    public Calculadora(String marca) {
        this.marca = marca;
    }

    public int sumar(int n1, int n2) {
        return n1+n2;
    }

    @Override
    public String toString() {
        return  marca + '\'';
    }
}
