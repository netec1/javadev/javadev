package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoDinamico.Ejemplo1;

public class Animal {

    private String nombre;

    public Animal(String nombre) {
        this.nombre = nombre;
    }

    public void realizarSonido() {
        System.out.println("El animal ha realizado un sonido");
    }

    @Override
    public String toString() {
        return nombre + '\'' ;
    }
}
