package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoEstatico.Ejemplo2;

public class Calculadora {
    // Sobrecarga para sumar dos enteros
    public int sumar(int a, int b) {
        return a + b;
    }

    // Sobrecarga para sumar tres enteros
    public int sumar(int a, int b, int c) {
        return a + b + c;
    }

    // Sobrecarga para sumar dos doubles
    public double sumar(double a, double b) {
        return a + b;
    }
    public static void main(String[] args) {
        Calculadora calc = new Calculadora();

        // Ejemplo de uso de la sobrecarga para sumar dos enteros
        int resultado1 = calc.sumar(5, 10);
        System.out.println("Suma de dos enteros: " + resultado1);

        // Ejemplo de uso de la sobrecarga para sumar tres enteros
        int resultado2 = calc.sumar(5, 10, 15);
        System.out.println("Suma de tres enteros: " + resultado2);

        // Ejemplo de uso de la sobrecarga para sumar dos doubles
        double resultado3 = calc.sumar(5.0, 10.0);
        System.out.println("Suma de dos doubles: " + resultado3);
    }

}

