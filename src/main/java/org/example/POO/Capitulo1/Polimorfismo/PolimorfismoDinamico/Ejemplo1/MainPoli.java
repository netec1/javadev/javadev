package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoDinamico.Ejemplo1;

public class MainPoli {
    public static void main(String[] args) {

        Perro animal = new Perro("tato");

        System.out.print("El perro : " + animal + " ");
        animal.realizarSonido();

    }
}
