package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoDinamico.Ejemplo2;

public class TestAnimales {
    public static void main(String[] args) {
        Animal miAnimal = new Animal();
        Animal miPerro = new Perro();
        Animal miGato = new Gato();

        miAnimal.hacerSonido();
        miPerro.hacerSonido();
        miGato.hacerSonido();
    }
}
