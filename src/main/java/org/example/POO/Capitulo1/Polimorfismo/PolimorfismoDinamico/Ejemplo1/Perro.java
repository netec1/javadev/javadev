package org.example.POO.Capitulo1.Polimorfismo.PolimorfismoDinamico.Ejemplo1;

public class Perro extends Animal{


    public Perro(String nombre) {
        super(nombre);
    }

    public void realizarSonido() {
        System.out.println("el perro hace guau-guau");
    }
}
