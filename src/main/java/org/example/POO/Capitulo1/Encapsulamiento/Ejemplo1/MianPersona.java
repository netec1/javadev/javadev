package org.example.POO.Capitulo1.Encapsulamiento.Ejemplo1;


public class MianPersona {
    public static void main(String[] args) {

        Persona persona = new Persona();
        persona.setNombre("Juan");
        persona.setEdad(20);
        persona.setDireccion("calle random");
        persona.setSexo('M');

        Persona persona2 = new Persona();
        persona2.setNombre("Juana");
        persona2.setEdad(30);
        persona2.setDireccion("calle random2");
        persona2.setSexo('F');

        Persona persona3 = new Persona();
        persona3.setNombre("Andres");
        persona3.setEdad(40);
        persona3.setDireccion("calle random3");
        persona3.setSexo('M');

        System.out.println(persona);
        System.out.println(persona2);
        System.out.println(persona3);
    }
}
