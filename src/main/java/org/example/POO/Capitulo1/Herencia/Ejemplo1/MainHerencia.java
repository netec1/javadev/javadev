package org.example.POO.Capitulo1.Herencia.Ejemplo1;

public class MainHerencia {

    public static void main(String[] args) {


        Programador programador = new Programador("Den", "Luevano", 20333, "Victor");
        Disenador disenador = new Disenador("Steph", "Poblano", 25000, "Marcelino");

        System.out.println("====Probando metodo heredado de Programdor");
        System.out.println(programador.getNombre());
        System.out.println(programador.getJefe());

        System.out.println("====Probando metodo heredado de Disenador");
        System.out.println(disenador.getNombre());
    }

}
