package org.example.POO.Capitulo1.Herencia.Ejemplo1;

public class Empleado {

    private String nombre;
    private String apellido;
    private double sueldo;
    private String jefe;

    public Empleado() {
    }

    public Empleado(String nombre, String apellido, double sueldo, String jefe) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sueldo = sueldo;
        this.jefe = jefe;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getJefe() {
        return jefe;
    }

    public void setJefe(String jefe) {
        this.jefe = jefe;
    }
}
