package org.example.POO.Capitulo11.ParallelStream.Practicas;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class ReduceExample {
    public static void main(String[] args) {
        // Crear una lista de números del 1 al 50
        List<Integer> numeros = IntStream.rangeClosed(1, 50)
                .boxed()
                .toList();

        // Usar reduce() para calcular la suma de los números
        int suma = numeros.stream()
                .reduce(0, Integer::sum);
        System.out.println("Suma: " + suma);

        // Crear una lista de cadenas
        List<String> cadenas = Arrays.asList("hola", "mundo", "como", "estas");

        // Usar reduce() para concatenar las cadenas en una sola cadena
        String concatenado = cadenas.stream()
                .reduce("", String::concat);
        System.out.println("Concatenación: " + concatenado);
    }
}
