package org.example.POO.Capitulo11.ParallelStream.OperacionesDeterminadas;

import java.util.List;
import java.util.stream.IntStream;

public class Principal {
    public static void main(String[] args) {
        // Creamos una lista de números
        List<Integer> numeros = IntStream.rangeClosed(1, 200)
                .mapToObj(t -> t)
                .toList();

        // Operaciones determinísticas
        System.out.println("--*--*Average--*--*");
        numeros.parallelStream()
                .mapToInt(t -> t)
                .average()
                .ifPresent(t -> System.out.println("paralelo: " + t));

        numeros.stream()
                .mapToInt(t -> t)
                .average()
                .ifPresent(t -> System.out.println("secuencial: " + t));

        System.out.println("--*--*Sum--*--*");
        int sumaT = numeros.parallelStream()
                .mapToInt(t -> t)
                .sum();
        System.out.println("suma total paralela: " + sumaT);

        int sumaS = numeros.stream()
                .mapToInt(t -> t)
                .sum();
        System.out.println("suma total secuencial: " + sumaS);
    }
}

