package org.example.POO.Capitulo11.ParallelStream.Practicas;

import java.util.List;
import java.util.stream.IntStream;

public class NonDeterministicOperationsExample {
    public static void main(String[] args) {
        // Crear una lista de números del 1 al 10
        List<Integer> numeros = IntStream.rangeClosed(1, 10)
                .boxed()
                .toList();

        // Usar findAny() con parallelStream()
        numeros.parallelStream()
                .filter(n -> n % 2 == 0)
                .findAny()
                .ifPresent(n -> System.out.println("Número par encontrado (paralelo): " + n));

        // Usar findAny() con stream()
        numeros.stream()
                .filter(n -> n % 2 == 0)
                .findAny()
                .ifPresent(n -> System.out.println("Número par encontrado (secuencial): " + n));
    }
}

