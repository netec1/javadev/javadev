package org.example.POO.Capitulo11.ParallelStream.Practicas;

import java.util.List;
import java.util.stream.IntStream;

public class ParallelStreamExample {
    public static void main(String[] args) {
        // Crear una lista de números del 1 al 100
        List<Integer> numeros = IntStream.rangeClosed(1, 100)
                .mapToObj(Integer::valueOf)
                .toList();

        // Usar parallelStream() para imprimir cada número y el nombre del hilo que lo procesa
        numeros.parallelStream()
                .forEach(numero -> System.out.println("Número: " + numero + ", procesado por: " + Thread.currentThread().getName()));
    }
}

