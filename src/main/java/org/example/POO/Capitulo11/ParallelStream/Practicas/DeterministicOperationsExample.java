package org.example.POO.Capitulo11.ParallelStream.Practicas;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DeterministicOperationsExample {
    public static void main(String[] args) {
        // Crear una lista de 200 números aleatorios
        List<Integer> numeros = new Random().ints(200, 0, 100)
                .boxed()
                .collect(Collectors.toList());

        // Calcular la suma usando parallelStream()
        int sumaParalela = numeros.parallelStream()
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println("Suma paralela: " + sumaParalela);

        // Calcular la suma usando stream()
        int sumaSecuencial = numeros.stream()
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println("Suma secuencial: " + sumaSecuencial);

        // Calcular el promedio usando parallelStream()
        double promedioParalelo = numeros.parallelStream()
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0.0);
        System.out.println("Promedio paralelo: " + promedioParalelo);

        // Calcular el promedio usando stream()
        double promedioSecuencial = numeros.stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0.0);
        System.out.println("Promedio secuencial: " + promedioSecuencial);
    }
}

