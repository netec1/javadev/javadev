package org.example.POO.Capitulo11.ParallelStream.Practicas;

import java.util.List;

public class ParallelReduceExample {
    public static void main(String[] args) {


        // Crear una lista de personas
        List<Persona> personas = List.of(
                new Persona("Ed", 28),
                new Persona("Caro", 29),
                new Persona("Liz", 30)
        );

        // Usar parallelStream() y reduce() para sumar las edades
        int sumaEdades = personas.parallelStream()
                .map(Persona::getEdad)
                .reduce(0, Integer::sum);
        System.out.println("Suma de edades (paralelo): " + sumaEdades);
    }
}
