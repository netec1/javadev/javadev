package org.example.POO.Capitulo11.ParallelStream.Reduce;

import java.util.List;
import java.util.stream.IntStream;

public class Principal {
    public static void main(String[] args) {
        // Creamos una lista de números
        List<Integer> numeros = IntStream.rangeClosed(1, 100)
                .mapToObj(t -> t)
                .toList();

        // Sumar todos los valores de la lista
        int resultado = numeros.stream()
                .reduce(0, (a, b) -> a + b);
        System.out.println("suma reduce: " + resultado);

        // Concatenar cadenas con reduce
        List<String> cadenas = List.of("hola", "mundo", "como", "estas");
        String concatenado = cadenas.stream()
                .reduce("", String::concat);
        System.out.println("concatenación reduce: " + concatenado);

        // Cambiar letras a mayúsculas y concatenarlas en una cadena
        String[] letras = {"a", "b", "c"};
        String letrasMayus = List.of(letras).stream()
                .reduce("", (a, b) -> a.toUpperCase() + b.toUpperCase());
        System.out.println("letras mayúsculas reduce: " + letrasMayus);
    }
}

