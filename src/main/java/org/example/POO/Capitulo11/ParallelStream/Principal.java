package org.example.POO.Capitulo11.ParallelStream;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Principal {
    public static void main(String[] args) {
        // Creamos una lista de números
        List<Integer> numeros = IntStream.rangeClosed(1, 5)
                .mapToObj(t -> t).toList();

        // El stream se ejecuta de forma paralela
        numeros.parallelStream()
                .forEach(t -> System.out.println(t + " mostrando usando el hilo: " + Thread.currentThread().getName()));
    }
}
