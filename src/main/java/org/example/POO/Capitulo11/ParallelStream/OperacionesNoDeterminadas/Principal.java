package org.example.POO.Capitulo11.ParallelStream.OperacionesNoDeterminadas;

import java.util.List;
import java.util.stream.IntStream;

public class Principal {
    public static void main(String[] args) {
        // Creamos una lista de números
        List<Integer> numeros = IntStream.rangeClosed(1, 10)
                .mapToObj(t -> t)
                .toList();

        // Obtener el primer número par que encuentres
        System.out.println("--*--*FindAny--*--*");
        numeros.stream()
                .filter(t -> t % 2 == 0)
                .findAny()
                .ifPresent(t -> System.out.println("secuencial " + t));

        numeros.parallelStream()
                .filter(t -> t % 2 == 0)
                .findAny()
                .ifPresent(t -> System.out.println("paralelo " + t));
    }
}

