package org.example.POO.Laboratorios.Laboratorio;

import java.util.Scanner;

public class Main {
    private static Banco banco; // Declaración de la variable estática

    public static void main(String[] args) {
        // Crear banco
        Domicilio domicilioBanco = new Domicilio("Calle Principal", 123, "Centro", "Ciudad", 12345);
        banco = new Banco("Banco Ejemplo", domicilioBanco, "RFC123456", "555-1234");

        // Ejecutar menú principal
        Scanner scanner = new Scanner(System.in);
        int opcion;
        do {
            System.out.println("Bienvenido al Banco Ejemplo");
            System.out.println("1. Agregar Cliente");
            System.out.println("2. Agregar Cuenta");
            System.out.println("3. Realizar Depósito");
            System.out.println("4. Realizar Retiro");
            System.out.println("5. Mostrar Clientes");
            System.out.println("6. Guardar Banco");
            System.out.println("7. Cargar Banco");
            System.out.println("0. Salir");
            System.out.print("Seleccione una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea

            switch (opcion) {
                case 1:
                    agregarCliente(scanner);
                    break;
                case 2:
                    agregarCuenta(scanner);
                    break;
                case 3:
                    realizarDeposito(scanner);
                    break;
                case 4:
                    realizarRetiro(scanner);
                    break;
                case 5:
                    mostrarClientes();
                    break;
                case 6:
                    guardarBanco();
                    break;
                case 7:
                    cargarBanco();
                    break;
                case 0:
                    System.out.println("Saliendo del sistema...");
                    break;
                default:
                    System.out.println("Opción no válida.");
            }
        } while (opcion != 0);
    }

    private static void agregarCliente(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numero = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("Nombre: ");
        String nombre = scanner.nextLine();
        System.out.print("Calle: ");
        String calle = scanner.nextLine();
        System.out.print("Número: ");
        int numeroCalle = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("Colonia: ");
        String colonia = scanner.nextLine();
        System.out.print("Estado: ");
        String estado = scanner.nextLine();
        System.out.print("Código Postal: ");
        int codigoPostal = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("RFC: ");
        String rfc = scanner.nextLine();
        System.out.print("Teléfono: ");
        String telefono = scanner.nextLine();
        System.out.print("Fecha de Nacimiento: ");
        String fechaNacimiento = scanner.nextLine();

        Domicilio domicilio = new Domicilio(calle, numeroCalle, colonia, estado, codigoPostal);
        Cliente cliente = new Cliente(numero, nombre, domicilio, rfc, telefono, fechaNacimiento);
        banco.getClientes().add(cliente);
        System.out.println("Cliente agregado con éxito.");
    }

    private static void agregarCuenta(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numeroCliente = scanner.nextInt();
        Cliente cliente = null;
        for (Cliente c : banco.getClientes()) {
            if (c.getNumero() == numeroCliente) {
                cliente = c;
                break;
            }
        }

        if (cliente != null) {
            System.out.print("Número de cuenta: ");
            int numeroCuenta = scanner.nextInt();
            System.out.print("Saldo inicial: ");
            double saldo = scanner.nextDouble();
            System.out.print("Tipo de cuenta (1: Ahorro, 2: Cheque): ");
            int tipoCuenta = scanner.nextInt();

            switch (tipoCuenta) {
                case 1:
                    System.out.print("Tasa de Interés Mensual: ");
                    double tasaInteresMensual = scanner.nextDouble();
                    CuentaDeAhorro cuentaAhorro = new CuentaDeAhorro(numeroCuenta, saldo, tasaInteresMensual);
                    cliente.getCuentas().add(cuentaAhorro);
                    System.out.println("Cuenta de ahorro agregada con éxito.");
                    break;
                case 2:
                    System.out.print("Costo de Manejo Mensual: ");
                    double costoManejoMensual = scanner.nextDouble();
                    CuentaDeCheque cuentaCheque = new CuentaDeCheque(numeroCuenta, saldo, costoManejoMensual);
                    cliente.getCuentas().add(cuentaCheque);
                    System.out.println("Cuenta de cheque agregada con éxito.");
                    break;
                default:
                    System.out.println("Tipo de cuenta no válido.");
            }
        } else {
            System.out.println("Cliente no encontrado.");
        }
    }

    private static void realizarDeposito(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numeroCliente = scanner.nextInt();
        Cliente cliente = null;
        for (Cliente c : banco.getClientes()) {
            if (c.getNumero() == numeroCliente) {
                cliente = c;
                break;
            }
        }

        if (cliente != null) {
            System.out.print("Número de cuenta: ");
            int numeroCuenta = scanner.nextInt();
            Cuenta cuenta = null;
            for (Cuenta c : cliente.getCuentas()) {
                if (c.getNumero() == numeroCuenta) {
                    cuenta = c;
                    break;
                }
            }

            if (cuenta != null) {
                System.out.print("Monto a depositar: ");
                double monto = scanner.nextDouble();
                cuenta.depositar(monto);
                System.out.println("Depósito realizado con éxito.");
            } else {
                System.out.println("Cuenta no encontrada.");
            }
        } else {
            System.out.println("Cliente no encontrado.");
        }
    }

    private static void realizarRetiro(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numeroCliente = scanner.nextInt();
        Cliente cliente = null;
        for (Cliente c : banco.getClientes()) {
            if (c.getNumero() == numeroCliente) {
                cliente = c;
                break;
            }
        }

        if (cliente != null) {
            System.out.print("Número de cuenta: ");
            int numeroCuenta = scanner.nextInt();
            Cuenta cuenta = null;
            for (Cuenta c : cliente.getCuentas()) {
                if (c.getNumero() == numeroCuenta) {
                    cuenta = c;
                    break;
                }
            }

            if (cuenta != null) {
                System.out.print("Monto a retirar: ");
                double monto = scanner.nextDouble();
                cuenta.retirar(monto);
                System.out.println("Retiro realizado con éxito.");
            } else {
                System.out.println("Cuenta no encontrada.");
            }
        } else {
            System.out.println("Cliente no encontrado.");
        }
    }

    private static void mostrarClientes() {
        for (Cliente cliente : banco.getClientes()) {
            System.out.println(cliente);
        }
    }

    private static void guardarBanco() {
        Persistencia.guardarBanco(banco, "banco.dat");
        System.out.println("Banco guardado con éxito.");
    }

    private static void cargarBanco() {
        banco = Persistencia.cargarBanco("banco.dat");
        if (banco != null) {
            System.out.println("Banco cargado con éxito.");
        } else {
            System.out.println("Error al cargar el banco.");
        }
    }
}
