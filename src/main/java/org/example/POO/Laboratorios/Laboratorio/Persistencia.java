package org.example.POO.Laboratorios.Laboratorio;

import java.io.*;

public class Persistencia {
    public static void guardarBanco(Banco banco, String nombreArchivo) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nombreArchivo))) {
            oos.writeObject(banco);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Banco cargarBanco(String nombreArchivo) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(nombreArchivo))) {
            return (Banco) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
