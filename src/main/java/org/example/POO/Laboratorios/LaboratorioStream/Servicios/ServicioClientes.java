package org.example.POO.Laboratorios.LaboratorioStream.Servicios;



import org.example.POO.Laboratorios.LaboratorioStream.Entidades.Cliente;

import java.util.List;

public interface ServicioClientes {
    boolean agregarCliente(Cliente cliente);

    boolean eliminarCliente(int numero);

    Cliente consultarCliente(int numero);

    List<Cliente> obtenerClientes();

    Cliente buscarClientePorRFC(String rfc);
}
