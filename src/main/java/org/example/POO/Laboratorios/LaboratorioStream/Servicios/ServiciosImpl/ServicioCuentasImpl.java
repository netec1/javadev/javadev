package org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServiciosImpl;

import org.example.POO.Laboratorios.LaboratorioStream.Entidades.Banco;
import org.example.POO.Laboratorios.LaboratorioStream.Entidades.Cuenta;
import org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServicioCuentas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServicioCuentasImpl implements ServicioCuentas {
    private List<Cuenta> cuentas = new ArrayList<>();

    public ServicioCuentasImpl(Banco banco) {
    }

    @Override
    public boolean agregarCuenta(Cuenta cuenta) {
        return cuentas.add(cuenta);
    }

    @Override
    public boolean cancelarCuenta(int numero) {
        return cuentas.removeIf(c -> c.getNumero() == numero);
    }

    @Override
    public boolean abonarCuenta(int numero, double abono) {
        return cuentas.stream()
                .filter(c -> c.getNumero() == numero)
                .findFirst()
                .map(c -> {
                    c.setSaldo(c.getSaldo() + abono);
                    return true;
                })
                .orElse(false);
    }

    @Override
    public boolean retirarNumerales(int numero, double cantidad) {
        return cuentas.stream()
                .filter(c -> c.getNumero() == numero && c.getSaldo() >= cantidad)
                .findFirst()
                .map(c -> {
                    c.setSaldo(c.getSaldo() - cantidad);
                    return true;
                })
                .orElse(false);
    }

    @Override
    public List<Cuenta> obtenerCuentas() {
        return cuentas.stream()
                .collect(Collectors.toList());
    }
}

