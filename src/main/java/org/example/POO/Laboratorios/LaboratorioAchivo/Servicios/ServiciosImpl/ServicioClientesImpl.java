package org.example.POO.Laboratorios.LaboratorioAchivo.Servicios.ServiciosImpl;

import org.example.POO.Laboratorios.LaboratorioAchivo.Entidades.Banco;
import org.example.POO.Laboratorios.LaboratorioAchivo.Entidades.Cliente;
import org.example.POO.Laboratorios.LaboratorioAchivo.Servicios.ServicioClientes;

import java.util.List;
import java.util.stream.Collectors;

public class ServicioClientesImpl implements ServicioClientes {
    private Banco banco;

    public ServicioClientesImpl(Banco banco) {
        this.banco = banco;
    }

    @Override
    public boolean agregarCliente(Cliente cliente) {
        return banco.getClientes().add(cliente);
    }

    @Override
    public boolean eliminarCliente(int numero) {
        return banco.getClientes().removeIf(c -> c.getNumero() == numero);
    }

    @Override
    public Cliente consultarCliente(int numero) {
        return banco.getClientes().stream()
                .filter(c -> c.getNumero() == numero)
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Cliente> obtenerClientes() {
        return banco.getClientes().stream()
                .collect(Collectors.toList());
    }

    @Override
    public Cliente buscarClientePorRFC(String rfc) {
        return banco.getClientes().stream()
                .filter(c -> c.getRfc().equals(rfc))
                .findFirst()
                .orElse(null);
    }
}

