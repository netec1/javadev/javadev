package org.example.POO.Laboratorios.LaboratorioAchivo;


import org.example.POO.Laboratorios.LaboratorioStream.Entidades.*;
import org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServicioClientes;
import org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServicioCuentas;
import org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServiciosImpl.ServicioClientesImpl;
import org.example.POO.Laboratorios.LaboratorioStream.Servicios.ServiciosImpl.ServicioCuentasImpl;

import java.util.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class Main {
    private static Banco banco;
    private static ServicioClientes servicioClientes;
    private static ServicioCuentas servicioCuentas;

    public static void main(String[] args) {
        // Crear banco y servicios
        Domicilio domicilioBanco = new Domicilio("Calle Principal", 123, "Centro", "Ciudad", 12345);
        banco = new Banco("Banco Ejemplo", domicilioBanco, "RFC123456", "555-1234");
        servicioClientes = new ServicioClientesImpl(banco);
        servicioCuentas = new ServicioCuentasImpl(banco);

        // Crear clientes
        crearClientes();

        // Leer cuentas del archivo y asignarlas a los clientes
        leerYAsignarCuentas("/home/carlos/Descargas/cuentas.txt");

        // Validar asignación de cuentas
        validarCuentas();

        // Ejecutar menú principal (opcional, si se desea mantener la funcionalidad interactiva)
        // ejecutarMenuPrincipal();
    }

    private static void crearClientes() {
        // Crear y agregar tres clientes
        Domicilio domicilio1 = new Domicilio("Calle 1", 101, "Colonia 1", "Estado 1", 11111);
        Domicilio domicilio2 = new Domicilio("Calle 2", 102, "Colonia 2", "Estado 2", 22222);
        Domicilio domicilio3 = new Domicilio("Calle 3", 103, "Colonia 3", "Estado 3", 33333);

        Cliente cliente1 = new Cliente(1, "Cliente 1", domicilio1, "RFC1", "555-1111", "01-01-1990");
        Cliente cliente2 = new Cliente(2, "Cliente 2", domicilio2, "RFC2", "555-2222", "02-02-1991");
        Cliente cliente3 = new Cliente(3, "Cliente 3", domicilio3, "RFC3", "555-3333", "03-03-1992");

        servicioClientes.agregarCliente(cliente1);
        servicioClientes.agregarCliente(cliente2);
        servicioClientes.agregarCliente(cliente3);
    }

    private static void leerYAsignarCuentas(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                linea = linea.trim();
                if (linea.isEmpty()) {
                    continue; // Saltar líneas vacías
                }
                String tipoCuenta = linea.substring(0, 2); // Obtener el tipo de cuenta (CA o CC)
                if (linea.startsWith("CA[") || linea.startsWith("CC[")) {
                    // Eliminar "CA[" o "CC[" y el último "]"
                    linea = linea.substring(3, linea.length() - 1).trim();
                    String[] partes = linea.split(",\\s*");

                    if (partes.length == 5) {
                        int numero = Integer.parseInt(partes[0].trim());
                        String fechaApertura = partes[1].trim();
                        double saldo = Double.parseDouble(partes[2].trim().replace(",", "."));
                        double parametroAdicional = Double.parseDouble(partes[3].trim().replace(",", "."));
                        int numeroCliente = Integer.parseInt(partes[4].trim());

                        if (tipoCuenta.equals("CA")) {
                            double tasaInteresMensual = parametroAdicional;
                            CuentaDeAhorro cuenta = new CuentaDeAhorro(numero, saldo, tasaInteresMensual);
                            servicioCuentas.agregarCuenta(cuenta);
                            Cliente cliente = servicioClientes.consultarCliente(numeroCliente);
                            if (cliente != null) {
                                cliente.getCuentas().add(cuenta);
                                System.out.println("Cuenta de ahorro agregada a Cliente " + numeroCliente + ": " + cuenta);
                            } else {
                                System.out.println("Cliente " + numeroCliente + " no encontrado.");
                            }
                        } else if (tipoCuenta.equals("CC")) {
                            double costoManejoMensual = parametroAdicional;
                            CuentaDeCheque cuenta = new CuentaDeCheque(numero, saldo, costoManejoMensual);
                            servicioCuentas.agregarCuenta(cuenta);
                            Cliente cliente = servicioClientes.consultarCliente(numeroCliente);
                            if (cliente != null) {
                                cliente.getCuentas().add(cuenta);
                                System.out.println("Cuenta de cheque agregada a Cliente " + numeroCliente + ": " + cuenta);
                            } else {
                                System.out.println("Cliente " + numeroCliente + " no encontrado.");
                            }
                        }
                    } else {
                        System.out.println("Formato de línea incorrecto: " + linea);
                    }
                } else {
                    System.out.println("Formato de línea incorrecto: " + linea);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error al convertir número: " + e.getMessage());
        }
    }

    private static void validarCuentas() {
        Cliente cliente1 = servicioClientes.consultarCliente(1);
        Cliente cliente2 = servicioClientes.consultarCliente(2);
        Cliente cliente3 = servicioClientes.consultarCliente(3);

        System.out.println("Validando cuentas de clientes...");

        if (cliente1 != null) {
            System.out.println("Cliente 1 tiene " + cliente1.getCuentas().size() + " cuentas.");
            if (cliente1.getCuentas().size() == 1) {
                System.out.println("Cliente 1 tiene 1 cuenta.");
            } else {
                System.out.println("Error: Cliente 1 no tiene 1 cuenta.");
            }
        } else {
            System.out.println("Error: Cliente 1 no encontrado.");
        }

        if (cliente2 != null) {
            System.out.println("Cliente 2 tiene " + cliente2.getCuentas().size() + " cuentas.");
            if (cliente2.getCuentas().size() == 2) {
                System.out.println("Cliente 2 tiene 2 cuentas.");
            } else {
                System.out.println("Error: Cliente 2 no tiene 2 cuentas.");
            }
        } else {
            System.out.println("Error: Cliente 2 no encontrado.");
        }

        if (cliente3 != null) {
            System.out.println("Cliente 3 tiene " + cliente3.getCuentas().size() + " cuentas.");
            if (cliente3.getCuentas().size() == 3) {
                System.out.println("Cliente 3 tiene 3 cuentas.");
            } else {
                System.out.println("Error: Cliente 3 no tiene 3 cuentas.");
                for (Cuenta cuenta : cliente3.getCuentas()) {
                    System.out.println("Cuenta de Cliente 3: " + cuenta);
                }
            }
        } else {
            System.out.println("Error: Cliente 3 no encontrado.");
        }
    }


}
