package org.example.POO.Laboratorios.LaboratorioParallelStreams.Utils;

import org.example.POO.Laboratorios.LaboratorioParallelStreams.Entidades.Cuenta;
import org.example.POO.Laboratorios.LaboratorioParallelStreams.Entidades.CuentaDeAhorro;
import org.example.POO.Laboratorios.LaboratorioParallelStreams.Entidades.CuentaDeCheque;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class LeerCuentasTask implements Callable<Map<Integer, List<Cuenta>>> {
    private String filePath;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public LeerCuentasTask(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public Map<Integer, List<Cuenta>> call() throws Exception {
        Map<Integer, List<Cuenta>> cuentasPorCliente = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                linea = linea.trim();
                if (linea.isEmpty()) {
                    continue; // Saltar líneas vacías
                }
                String tipoCuenta = linea.substring(0, 2); // Obtener el tipo de cuenta (CA o CC)
                if (linea.startsWith("CA[") || linea.startsWith("CC[")) {
                    // Eliminar "CA[" o "CC[" y el último "]"
                    linea = linea.substring(3, linea.length() - 1).trim();
                    String[] partes = linea.split(",\\s*");

                    if (partes.length == 5) {
                        int numero = Integer.parseInt(partes[0].trim());
                        LocalDate fechaApertura = LocalDate.parse(partes[1].trim(), formatter);
                        double saldo = Double.parseDouble(partes[2].trim().replace(",", "."));
                        double parametroAdicional = Double.parseDouble(partes[3].trim().replace(",", "."));
                        int numeroCliente = Integer.parseInt(partes[4].trim());

                        Cuenta cuenta;
                        if (tipoCuenta.equals("CA")) {
                            double tasaInteresMensual = parametroAdicional;
                            cuenta = new CuentaDeAhorro(numero, saldo, tasaInteresMensual);
                            cuenta.setFechaApertura(fechaApertura);
                        } else {
                            double costoManejoMensual = parametroAdicional;
                            cuenta = new CuentaDeCheque(numero, saldo, costoManejoMensual);
                            cuenta.setFechaApertura(fechaApertura);
                        }

                        cuentasPorCliente.computeIfAbsent(numeroCliente, k -> new ArrayList<>()).add(cuenta);
                    } else {
                        System.out.println("Formato de línea incorrecto: " + linea);
                    }
                } else {
                    System.out.println("Formato de línea incorrecto: " + linea);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error al convertir número: " + e.getMessage());
        }

        return cuentasPorCliente;
    }
}
