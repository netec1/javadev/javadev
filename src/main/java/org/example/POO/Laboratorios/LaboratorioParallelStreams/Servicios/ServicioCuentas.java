package org.example.POO.Laboratorios.LaboratorioParallelStreams.Servicios;

import org.example.POO.Laboratorios.LaboratorioParallelStreams.Entidades.Cuenta;

import java.util.List;

public interface ServicioCuentas {
    boolean agregarCuenta(Cuenta cuenta);

    boolean cancelarCuenta(int numero);

    boolean abonarCuenta(int numero, double abono);

    boolean retirarNumerales(int numero, double cantidad);

    List<Cuenta> obtenerCuentas();
}
