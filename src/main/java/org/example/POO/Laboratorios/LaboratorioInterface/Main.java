package org.example.POO.Laboratorios.LaboratorioInterface;


import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServicioClientes;
import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServicioCuentas;
import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServiciosImpl.ServicioClientesImpl;
import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServiciosImpl.ServicioCuentasImpl;
import org.example.POO.Laboratorios.LaboratorioInterface.Entidades.*;


import java.util.Scanner;

public class Main {
    private static Banco banco;
    private static ServicioClientes servicioClientes;
    private static ServicioCuentas servicioCuentas;

    public static void main(String[] args) {
        // Crear banco y servicios
        Domicilio domicilioBanco = new Domicilio("Calle Principal", 123, "Centro", "Ciudad", 12345);
        banco = new Banco("Banco Ejemplo", domicilioBanco, "RFC123456", "555-1234");
        servicioClientes = new ServicioClientesImpl(banco);
        servicioCuentas = new ServicioCuentasImpl(banco);

        // Ejecutar menú principal
        Scanner scanner = new Scanner(System.in);
        int opcion;
        do {
            System.out.println("\nBienvenido al Banco Ejemplo");
            System.out.println("1. Agregar Cliente");
            System.out.println("2. Agregar Cuenta");
            System.out.println("3. Realizar Depósito");
            System.out.println("4. Realizar Retiro");
            System.out.println("5. Mostrar Clientes");
            System.out.println("0. Salir");
            System.out.print("Seleccione una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine(); // Consumir el salto de línea

            switch (opcion) {
                case 1:
                    agregarCliente(scanner);
                    break;
                case 2:
                    agregarCuenta(scanner);
                    break;
                case 3:
                    realizarDeposito(scanner);
                    break;
                case 4:
                    realizarRetiro(scanner);
                    break;
                case 5:
                    mostrarClientes();
                    break;
                case 0:
                    System.out.println("Saliendo del sistema...");
                    break;
                default:
                    System.out.println("Opción no válida.");
            }
        } while (opcion != 0);
        scanner.close();
    }

    private static void agregarCliente(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numero = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("Nombre: ");
        String nombre = scanner.nextLine();
        System.out.print("Calle: ");
        String calle = scanner.nextLine();
        System.out.print("Número: ");
        int numeroCalle = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("Colonia: ");
        String colonia = scanner.nextLine();
        System.out.print("Estado: ");
        String estado = scanner.nextLine();
        System.out.print("Código Postal: ");
        int codigoPostal = scanner.nextInt();
        scanner.nextLine(); // Consumir el salto de línea
        System.out.print("RFC: ");
        String rfc = scanner.nextLine();
        System.out.print("Teléfono: ");
        String telefono = scanner.nextLine();
        System.out.print("Fecha de Nacimiento: ");
        String fechaNacimiento = scanner.nextLine();

        Domicilio domicilio = new Domicilio(calle, numeroCalle, colonia, estado, codigoPostal);
        Cliente cliente = new Cliente(numero, nombre, domicilio, rfc, telefono, fechaNacimiento);
        if (servicioClientes.agregarCliente(cliente)) {
            System.out.println("Cliente agregado con éxito.");
        } else {
            System.out.println("Error al agregar cliente.");
        }
    }

    private static void agregarCuenta(Scanner scanner) {
        System.out.print("Número de cliente: ");
        int numeroCliente = scanner.nextInt();
        Cliente cliente = servicioClientes.consultarCliente(numeroCliente);

        if (cliente != null) {
            System.out.print("Número de cuenta: ");
            int numeroCuenta = scanner.nextInt();
            System.out.print("Saldo inicial: ");
            double saldo = scanner.nextDouble();
            System.out.print("Tipo de cuenta (1: Ahorro, 2: Cheque): ");
            int tipoCuenta = scanner.nextInt();

            Cuenta cuenta;
            if (tipoCuenta == 1) {
                System.out.print("Tasa de Interés Mensual: ");
                double tasaInteresMensual = scanner.nextDouble();
                cuenta = new CuentaDeAhorro(numeroCuenta, saldo, tasaInteresMensual);
            } else {
                System.out.print("Costo de Manejo Mensual: ");
                double costoManejoMensual = scanner.nextDouble();
                cuenta = new CuentaDeCheque(numeroCuenta, saldo, costoManejoMensual);
            }
            cliente.getCuentas().add(cuenta);
            if (servicioCuentas.agregarCuenta(cuenta)) {
                System.out.println("Cuenta agregada con éxito.");
            } else {
                System.out.println("Error al agregar la cuenta.");
            }
        } else {
            System.out.println("Cliente no encontrado.");
        }
    }


    private static void realizarDeposito(Scanner scanner) {
        System.out.print("Número de cuenta: ");
        int numeroCuenta = scanner.nextInt();
        System.out.print("Monto a depositar: ");
        double monto = scanner.nextDouble();

        if (servicioCuentas.abonarCuenta(numeroCuenta, monto)) {
            System.out.println("Depósito realizado con éxito.");
        } else {
            System.out.println("Error al realizar el depósito.");
        }
    }

    private static void realizarRetiro(Scanner scanner) {
        System.out.print("Número de cuenta: ");
        int numeroCuenta = scanner.nextInt();
        System.out.print("Monto a retirar: ");
        double monto = scanner.nextDouble();

        if (servicioCuentas.retirarNumerales(numeroCuenta, monto)) {
            System.out.println("Retiro realizado con éxito.");
        } else {
            System.out.println("Error al realizar el retiro o saldo insuficiente.");
        }
    }

    private static void mostrarClientes() {
        for (Cliente cliente : servicioClientes.obtenerClientes()) {
            System.out.println(cliente);
            System.out.println("Cuentas del Cliente:");
            for (Cuenta cuenta : cliente.getCuentas()) {
                System.out.println("\t" + cuenta);
            }
        }
    }

}

