package org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServiciosImpl;

import org.example.POO.Laboratorios.LaboratorioInterface.Entidades.Banco;
import org.example.POO.Laboratorios.LaboratorioInterface.Entidades.Cliente;
import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServicioClientes;


import java.util.List;

public class ServicioClientesImpl implements ServicioClientes {
    private Banco banco;

    public ServicioClientesImpl(Banco banco) {
        this.banco = banco;
    }

    @Override
    public boolean agregarCliente(Cliente cliente) {
        return banco.getClientes().add(cliente);
    }

    @Override
    public boolean eliminarCliente(int numero) {
        return banco.getClientes().removeIf(c -> c.getNumero() == numero);
    }

    @Override
    public Cliente consultarCliente(int numero) {
        return banco.getClientes().stream()
                .filter(c -> c.getNumero() == numero)
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Cliente> obtenerClientes() {
        return banco.getClientes();
    }

    @Override
    public Cliente buscarClientePorRFC(String rfc) {
        return banco.getClientes().stream()
                .filter(c -> c.getRfc().equals(rfc))
                .findFirst()
                .orElse(null);
    }
}

