package org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServiciosImpl;

import org.example.POO.Laboratorios.LaboratorioInterface.Entidades.Banco;
import org.example.POO.Laboratorios.LaboratorioInterface.Entidades.Cuenta;
import org.example.POO.Laboratorios.LaboratorioInterface.Servicios.ServicioCuentas;

import java.util.ArrayList;
import java.util.List;

public class ServicioCuentasImpl implements ServicioCuentas {
    private List<Cuenta> cuentas = new ArrayList<>();

    public ServicioCuentasImpl(Banco banco) {
    }

    @Override
    public boolean agregarCuenta(Cuenta cuenta) {
        return cuentas.add(cuenta);
    }

    @Override
    public boolean cancelarCuenta(int numero) {
        return cuentas.removeIf(c -> c.getNumero() == numero);
    }

    @Override
    public boolean abonarCuenta(int numero, double abono) {
        for (Cuenta c : cuentas) {
            if (c.getNumero() == numero) {
                c.setSaldo(c.getSaldo() + abono);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retirarNumerales(int numero, double cantidad) {
        for (Cuenta c : cuentas) {
            if (c.getNumero() == numero && c.getSaldo() >= cantidad) {
                c.setSaldo(c.getSaldo() - cantidad);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Cuenta> obtenerCuentas() {
        return new ArrayList<>(cuentas);
    }
}

