package org.example.POO.Laboratorios.LaboratorioFechas.Servicios;

import org.example.POO.Laboratorios.LaboratorioFechas.Entidades.Cuenta;

import java.util.List;

public interface ServicioCuentas {
    boolean agregarCuenta(Cuenta cuenta);

    boolean cancelarCuenta(int numero);

    boolean abonarCuenta(int numero, double abono);

    boolean retirarNumerales(int numero, double cantidad);

    List<Cuenta> obtenerCuentas();
}
