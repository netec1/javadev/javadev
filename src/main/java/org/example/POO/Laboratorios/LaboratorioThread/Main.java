package org.example.POO.Laboratorios.LaboratorioThread;


import org.example.POO.Laboratorios.LaboratorioThread.Entidades.Banco;
import org.example.POO.Laboratorios.LaboratorioThread.Entidades.Cliente;
import org.example.POO.Laboratorios.LaboratorioThread.Entidades.Cuenta;
import org.example.POO.Laboratorios.LaboratorioThread.Entidades.Domicilio;
import org.example.POO.Laboratorios.LaboratorioThread.Servicios.ServicioClientes;
import org.example.POO.Laboratorios.LaboratorioThread.Servicios.ServicioCuentas;
import org.example.POO.Laboratorios.LaboratorioThread.Servicios.ServiciosImpl.ServicioClientesImpl;
import org.example.POO.Laboratorios.LaboratorioThread.Servicios.ServiciosImpl.ServicioCuentasImpl;
import org.example.POO.Laboratorios.LaboratorioThread.Utils.LeerCuentasTask;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    private static Banco banco;
    private static ServicioClientes servicioClientes;
    private static ServicioCuentas servicioCuentas;

    public static void main(String[] args) {
        // Crear banco y servicios
        Domicilio domicilioBanco = new Domicilio("Calle Principal", 123, "Centro", "Ciudad", 12345);
        banco = new Banco("Banco Ejemplo", domicilioBanco, "RFC123456", "555-1234");
        servicioClientes = new ServicioClientesImpl(banco);
        servicioCuentas = new ServicioCuentasImpl(banco);

        // Crear clientes
        crearClientes();

        // Crear un ExecutorService para la lectura del archivo
        ExecutorService executor = Executors.newSingleThreadExecutor();
        LeerCuentasTask tarea = new LeerCuentasTask("/home/carlos/Descargas/cuentas.txt");

        try {
            // Enviar la tarea para que se ejecute y obtener un Future
            Future<Map<Integer, List<Cuenta>>> future = executor.submit(tarea);

            // Obtener el resultado de la tarea
            Map<Integer, List<Cuenta>> cuentasPorCliente = future.get();

            // Asignar las cuentas a los clientes
            asignarCuentasAClientes(cuentasPorCliente);

            // Validar asignación de cuentas
            validarCuentas();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }

        // Ejecutar menú principal (opcional, si se desea mantener la funcionalidad interactiva)
        // ejecutarMenuPrincipal();
    }

    private static void crearClientes() {
        // Crear y agregar tres clientes
        Domicilio domicilio1 = new Domicilio("Calle 1", 101, "Colonia 1", "Estado 1", 11111);
        Domicilio domicilio2 = new Domicilio("Calle 2", 102, "Colonia 2", "Estado 2", 22222);
        Domicilio domicilio3 = new Domicilio("Calle 3", 103, "Colonia 3", "Estado 3", 33333);

        Cliente cliente1 = new Cliente(1, "Cliente 1", domicilio1, "RFC1", "555-1111", LocalDate.parse("01-01-1990", DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        Cliente cliente2 = new Cliente(2, "Cliente 2", domicilio2, "RFC2", "555-2222", LocalDate.parse("02-02-1991", DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        Cliente cliente3 = new Cliente(3, "Cliente 3", domicilio3, "RFC3", "555-3333", LocalDate.parse("03-03-1992", DateTimeFormatter.ofPattern("dd-MM-yyyy")));

        servicioClientes.agregarCliente(cliente1);
        servicioClientes.agregarCliente(cliente2);
        servicioClientes.agregarCliente(cliente3);
    }

    private static void asignarCuentasAClientes(Map<Integer, List<Cuenta>> cuentasPorCliente) {
        for (Map.Entry<Integer, List<Cuenta>> entry : cuentasPorCliente.entrySet()) {
            int numeroCliente = entry.getKey();
            List<Cuenta> cuentas = entry.getValue();
            Cliente cliente = servicioClientes.consultarCliente(numeroCliente);
            if (cliente != null) {
                cliente.getCuentas().addAll(cuentas);
                System.out.println("Cuentas asignadas a Cliente " + numeroCliente + ": " + cuentas);
            } else {
                System.out.println("Cliente " + numeroCliente + " no encontrado.");
            }
        }
    }

    private static void validarCuentas() {
        Cliente cliente1 = servicioClientes.consultarCliente(1);
        Cliente cliente2 = servicioClientes.consultarCliente(2);
        Cliente cliente3 = servicioClientes.consultarCliente(3);

        System.out.println("Validando cuentas de clientes...");

        if (cliente1 != null) {
            System.out.println("Cliente 1 tiene " + cliente1.getCuentas().size() + " cuentas.");
            if (cliente1.getCuentas().size() == 1) {
                System.out.println("Cliente 1 tiene 1 cuenta.");
            } else {
                System.out.println("Error: Cliente 1 no tiene 1 cuenta.");
            }
        } else {
            System.out.println("Error: Cliente 1 no encontrado.");
        }

        if (cliente2 != null) {
            System.out.println("Cliente 2 tiene " + cliente2.getCuentas().size() + " cuentas.");
            if (cliente2.getCuentas().size() == 2) {
                System.out.println("Cliente 2 tiene 2 cuentas.");
            } else {
                System.out.println("Error: Cliente 2 no tiene 2 cuentas.");
            }
        } else {
            System.out.println("Error: Cliente 2 no encontrado.");
        }

        if (cliente3 != null) {
            System.out.println("Cliente 3 tiene " + cliente3.getCuentas().size() + " cuentas.");
            if (cliente3.getCuentas().size() == 3) {
                System.out.println("Cliente 3 tiene 3 cuentas.");
            } else {
                System.out.println("Error: Cliente 3 no tiene 3 cuentas.");
                for (Cuenta cuenta : cliente3.getCuentas()) {
                    System.out.println("Cuenta de Cliente 3: " + cuenta);
                }
            }
        } else {
            System.out.println("Error: Cliente 3 no encontrado.");
        }
    }
}

