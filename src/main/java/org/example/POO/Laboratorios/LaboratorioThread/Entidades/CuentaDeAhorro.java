package org.example.POO.Laboratorios.LaboratorioThread.Entidades;


public class CuentaDeAhorro extends Cuenta {
    private double tasaInteresMensual;

    public CuentaDeAhorro(int numero, double saldo, double tasaInteresMensual) {
        super(numero, saldo);
        this.tasaInteresMensual = tasaInteresMensual;
    }

    public double getTasaInteresMensual() { return tasaInteresMensual; }
    public void setTasaInteresMensual(double tasaInteresMensual) { this.tasaInteresMensual = tasaInteresMensual; }

    public double calcularIntereses() {
        return saldo * tasaInteresMensual / 100;
    }

    @Override
    public String toString() {
        return "CuentaDeAhorro{" +
                "tasaInteresMensual=" + tasaInteresMensual +
                ", numero=" + numero +
                ", fechaApertura='" + fechaApertura + '\'' +
                ", saldo=" + saldo +
                ", fechaCancelacion='" + fechaCancelacion + '\'' +
                '}';
    }
}

