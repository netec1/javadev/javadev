package org.example.POO.Laboratorios.LaboratorioColecciones.Servicios;



import org.example.POO.Laboratorios.LaboratorioColecciones.Entidades.Cliente;

import java.util.TreeSet;

public interface ServicioClientes {
    boolean agregarCliente(Cliente cliente);

    boolean eliminarCliente(int numero);

    Cliente consultarCliente(int numero);

    TreeSet<Cliente> obtenerClientes();

    Cliente buscarClientePorRFC(String rfc);
}
